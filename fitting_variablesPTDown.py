from ROOT import TChain, TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, RooFormulaVar, RooDataSet, RooDataHist, RooCategory, RooKeysPdf, TLorentzVector, TList, gROOT, gDirectory, TAxis, RooPlot, RooHist, RooPlotable, RooCurve,TGLHistPainter, TPad, TGraph, TDirectory

import ROOT as r
from array import array
import numpy as np
import os
import json
def storeSWeightsDown(polarity, low_mass, high_mass, input_file, sliceNumber):

    file1 = TFile.Open(input_file)
    
    #get Objects
    r.gStyle.SetOptStat(0)
   
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
    PTSlicesD = json.load( open( "PTSlicesD"+polarity+".json" ) )
    
    import math

    #r.gROOT.SetBatch(True)
    #r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
    
    Jpsi_M = r.RooRealVar("J_psi_1S_M","Mass J/\psi",low_mass,high_mass,"#it MeV/{c}^{2}")
    muplus_Assoc = r.RooRealVar("muplus_Assoc","Matched",0,1," ")
    muminus_Assoc = r.RooRealVar("muminus_Assoc","Matched",0,1," ")

    Bwidth = (high_mass-low_mass)/(Jpsi_M.getBins());
    print(Bwidth)
    for i in range(0,sliceNumber):
        d_Plus = r.RooDataSet();
        file1.GetObject("data_Plus"+ str(PTSlicesD["PTlow"+str(i)]), d_Plus);
        d_PlusM = r.RooDataSet();
        file1.GetObject("data_MatchedPlus"+ str(PTSlicesD["PTlow"+str(i)]), d_PlusM);
        d_Minus = r.RooDataSet();
        file1.GetObject("data_Minus"+ str(PTSlicesD["PTlow"+str(i)]), d_Minus);
        d_MinusM = r.RooDataSet();
        file1.GetObject("data_MatchedMinus"+ str(PTSlicesD["PTlow"+str(i)]), d_MinusM);
    
        dataHPlus = r.RooDataHist("dataHPlus","dataHPlus", r.RooArgSet(Jpsi_M),d_Plus);
        dataHPlusMatched = r.RooDataHist("dataHPlusMatched","dataHPlusMatched", r.RooArgSet(Jpsi_M),d_PlusM);
        dataHMinus = r.RooDataHist("dataHMinus","dataHMinus", r.RooArgSet(Jpsi_M), d_Minus);
        dataHMinusMatched = r.RooDataHist("dataHMinusMatched","dataHMinusMatched", r.RooArgSet(Jpsi_M), d_MinusM);
        #max and min for signal and background fits applied
        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e8);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e8);
        #range and start point of fitting parameters defined
        #FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
        #frac = r.RooFormulaVar("frac","(1.0-FC)",r.RooArgList(FC));
        
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,1.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0);  
        elif (PTSlicesD["PTlow"+str(i)] == 3500):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,120.0); 
        elif(PTSlicesD["PTlow"+str(i)] == 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",6.0,0.0,125.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,125.0); 
        elif(PTSlicesD["PTlow"+str(i)] == 3000):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,120.0); 
        elif(PTSlicesD["PTlow"+str(i)] == 2200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,115.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",5.0,0.0,115.0); 
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,115.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,115.0)
        n = r.RooRealVar("n1","n of both CB",0.1)
        if (PTSlicesD["PTlow"+str(i)] == 2200):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",5,0.,10.)
            alpha2 = r.RooRealVar("alpha2","alpha of second CB",4,0.,10.)

        else:
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",4,0.,10.)
            alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        if (PTSlicesD["PTlow"+str(i)] == 2600):
            frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
        else:
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        if (PTSlicesD["PTlow"+str(i)] == 2200):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))          
        else:                     
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
        
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
        
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHPlus,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1)); 
        
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
        
        pad.Draw()
        pad.cd()
        
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
        
        #Data plotted on frame
        d_Plus.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
        
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
        
        
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
              
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
        
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
        
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
        
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
        
        c.Modified()
        c.Update()
        
        c.SaveAs("PlotMassJ" + polarity + "PlusFull" +  str(PTSlicesD["PTlow"+str(i)]) + ".png");
        if (PTSlicesD["PTlow"+str(i)] == 2600):
            sData = r.RooStats.SPlot("sData","An sPlot", d_Plus, model, r.RooArgList(nsig,nbkg))
            sData.GetSDataSet().write("sweightsJ" + polarity + "Plus"+ str(PTSlicesD["PTlow"+str(i)])+".txt")

        
        nsig =  r.RooRealVar("nsig", "nsig", 1e5, 0, 1e8);
        nbkg =  r.RooRealVar("nbkg", "nbkg", 1e5, 0, 6e8);
        
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)] == 3000 or 2200):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,1.0,100.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,130.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",5.0,0.0,130.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,120.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",6.0,0.0,120.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1800):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",6.0,0.0,100.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",6.0,0.0,100.0)
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",5.0,3.0,100.0)
        n = r.RooRealVar("n1","n of both CB",0.85)
        alpha1 = r.RooRealVar("alpha1","alpha of first CB",3,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        if (PTSlicesD["PTlow"+str(i)] == 1800):
            frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1200):
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2200):
            frac = r.RooRealVar("frac","frac",0.05,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2600):
            frac = r.RooRealVar("frac","frac",0.4,0.,1.0)
        else:
            frac = r.RooRealVar("frac","frac",0.2,0.,1.0)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        if (PTSlicesD["PTlow"+str(i)] == 2600):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(CB1,gauss2),r.RooArgList(frac))
        else:
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))

        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
        
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
        
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHPlusMatched,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
        
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
        
        pad.Draw()
        pad.cd()
        
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
        
        #Data plotted on frame
        d_PlusM.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
        
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
        
        
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
              
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
        
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
        
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
        
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
        
        c.Modified()
        c.Update()
        
        c.SaveAs("PlotMassJ" + polarity + "PlusMatched" + str(PTSlicesD["PTlow"+str(i)])+".png");

        #SWeights calculated
        #sData = r.RooStats.SPlot("sData","An sPlot", d_PlusM, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "PlusMatched"+ str(PTSlicesD["PTlow"+str(i)])+".txt")
        """

        #max and min for signal and background fits applied
        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e9);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e9);
        #range and start point of fitting parameters defined
        #FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
        #frac = r.RooFormulaVar("frac","(1.0-FC)",r.RooArgList(FC));
        

        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)]==1800):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,110.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,110.0);
        elif (PTSlicesD["PTlow"+str(i)]==2200):
            sigma1 = r.RooRealVar("sigma1","sigma1",12.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,120.0);
        elif(PTSlicesD["PTlow"+str(i)] == 4200):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,2.0,125.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,1.0,125.0);
        elif(PTSlicesD["PTlow"+str(i)] == 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0);  
        elif(PTSlicesD["PTlow"+str(i)] == 3000):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0);  
        elif(PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",12.0,0.0,130.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,130.0);
        elif(PTSlicesD["PTlow"+str(i)] == 5400):
            sigma1 = r.RooRealVar("sigma1","sigma1",12.0,0.0,130.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,130.0);
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,2.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,1.0,100.0);
        n = r.RooRealVar("n1","n of both CB",0.9)
        if (PTSlicesD["PTlow"+str(i)]==1200):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",0.525,0.,10.)
        elif (PTSlicesD["PTlow"+str(i)]==5400):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",0.525,0.,10.)
        else:
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",1.9,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
        
        if (PTSlicesD["PTlow"+str(i)]==1200 or PTSlicesD["PTlow"+str(i)]==5400):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(CB1,gauss2),r.RooArgList(frac))
        else:
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
        
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
        
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHMinus,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
        
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
        
        pad.Draw()
        pad.cd()
        
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
        
        #Data plotted on frame
        d_Minus.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
        
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
        
        
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
              
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
        
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
        
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
        
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
        
        c.Modified()
        c.Update()
        
        c.SaveAs("PlotMassJ" + polarity + "MinusFull" + str(PTSlicesD["PTlow"+str(i)]) + ".png");

        #SWeights calculated

        #sData = r.RooStats.SPlot("sData","An sPlot", d_Minus, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "Minus" + str(PTSlicesD["PTlow"+str(i)]) + ".txt")

        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e9);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e9);

        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,0.0,100.0)
        elif(PTSlicesD["PTlow"+str(i)] == 1800):
            sigma1 = r.RooRealVar("sigma1","sigma1",15.0,2.0,125.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,1.0,125.0)
        elif(PTSlicesD["PTlow"+str(i)] == 2240):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,120.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",3.0,0.0,120.0)
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,130.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,0.0,130.0)
        n = r.RooRealVar("n1","n of both CB",0.9)
        alpha1 = r.RooRealVar("alpha1","alpha of first CB",4,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        gauss1 = r.RooGaussian("gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("gauss2","gauss2(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma2,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
        
        print("test");
        SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        print("test");
       
        
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
        
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
        
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHMinusMatched,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
        
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
        
        pad.Draw()
        pad.cd()
        
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
        
        #Data plotted on frame
        d_MinusM.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
        
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
        
        
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
              
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
        
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
        
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
        
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
        
        c.Modified()
        c.Update()
        
        c.SaveAs("PlotMassJ" + polarity + "MinusMatched" + str(PTSlicesD["PTlow"+str(i)]) + ".png");
        
        #SWeights calculated
        #sData = r.RooStats.SPlot("sData","An sPlot", d_MinusM, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "MinusMatched" + str(PTSlicesD["PTlow"+str(i)])+".txt")
        """
def storeSWeightsDownSystematics(polarity, low_mass, high_mass, input_file, sliceNumber):

    file1 = TFile.Open(input_file)
  
    #get Objects
    r.gStyle.SetOptStat(0)
  
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
    PTSlicesD = json.load( open( "PTSlicesD"+polarity+"Syst.json" ) )
  
    import math
    #r.gROOT.SetBatch(True)
    #r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
  
    Jpsi_M = r.RooRealVar("J_psi_1S_M","Mass J/\psi",low_mass,high_mass,"#it MeV/{c}^{2}")
    muplus_Assoc = r.RooRealVar("muplus_Assoc","Matched",0,1," ")
    muminus_Assoc = r.RooRealVar("muminus_Assoc","Matched",0,1," ")
    Bwidth = (high_mass-low_mass)/(Jpsi_M.getBins());
    print(Bwidth)
    for i in range(0,sliceNumber):
       
        d_Plus = r.RooDataSet();
        file1.GetObject("data_Plus"+ str(PTSlicesD["PTlow"+str(i)]), d_Plus);
        d_PlusM = r.RooDataSet();
        file1.GetObject("data_MatchedPlus"+ str(PTSlicesD["PTlow"+str(i)]), d_PlusM);
        d_Minus = r.RooDataSet();
        file1.GetObject("data_Minus"+ str(PTSlicesD["PTlow"+str(i)]), d_Minus);
        d_MinusM = r.RooDataSet();
        file1.GetObject("data_MatchedMinus"+ str(PTSlicesD["PTlow"+str(i)]), d_MinusM);
  
        dataHPlus = r.RooDataHist("dataHPlus","dataHPlus", r.RooArgSet(Jpsi_M),d_Plus);
        dataHPlusMatched = r.RooDataHist("dataHPlusMatched","dataHPlusMatched", r.RooArgSet(Jpsi_M),d_PlusM);
        dataHMinus = r.RooDataHist("dataHMinus","dataHMinus", r.RooArgSet(Jpsi_M), d_Minus);
        dataHMinusMatched = r.RooDataHist("dataHMinusMatched","dataHMinusMatched", r.RooArgSet(Jpsi_M), d_MinusM);
        #max and min for signal and background fits applied
        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e8);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e8);
        #range and start point of fitting parameters defined
        #FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
        #frac = r.RooFormulaVar("frac","(1.0-FC)",r.RooArgList(FC));
        
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3050,3250.0);
        if (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,1.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0); 
        elif (PTSlicesD["PTlow"+str(i)] == 1700):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0);
        elif(PTSlicesD["PTlow"+str(i)] == 2100):
            sigma1 = r.RooRealVar("sigma1","sigma1",6.0,0.0,125.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,125.0);
        elif(PTSlicesD["PTlow"+str(i)] == 2300):
            sigma1 = r.RooRealVar("sigma1","sigma1",16.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",10.0,0.0,120.0);
        elif(PTSlicesD["PTlow"+str(i)] == 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",15.0,2.0,125.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,2.0,125.0);
        elif(PTSlicesD["PTlow"+str(i)] == 3000):
            sigma1 = r.RooRealVar("sigma1","sigma1",16.0,2.0,130.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",10.0,2.0,130.0);
        elif(PTSlicesD["PTlow"+str(i)] == 3400):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,120.0);
        elif(PTSlicesD["PTlow"+str(i)] == 3900):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,120.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,120.0);
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,115.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,115.0)
        n = r.RooRealVar("n1","n of both CB",0.1)
        if (PTSlicesD["PTlow"+str(i)] == 2200):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",5,0.,10.)
            alpha2 = r.RooRealVar("alpha2","alpha of second CB",4,0.,10.)
        else:
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",4,0.,10.)
            alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.25,0.,10.)
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        if (PTSlicesD["PTlow"+str(i)] == 2600):
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        else:
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        if (PTSlicesD["PTlow"+str(i)] == 3000):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(CB2,gauss1),r.RooArgList(frac))         
        else:                   
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
      
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
      
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
      
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHPlus,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
      
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
      
        pad.Draw()
        pad.cd()
      
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
      
        #Data plotted on frame
        d_Plus.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
      
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
      
      
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
            
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
      
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
      
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
      
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
      
        c.Modified()
        c.Update()
      
        c.SaveAs("PlotMassJ" + polarity + "PlusFull" +  str(PTSlicesD["PTlow"+str(i)]) + "Syst.png");
        #sData = r.RooStats.SPlot("sData","An sPlot", d_Plus, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "Plus"+ str(PTSlicesD["PTlow"+str(i)])+"Syst.txt")
        
        
       
        nsig =  r.RooRealVar("nsig", "nsig", 1e5, 0, 1e8);
        nbkg =  r.RooRealVar("nbkg", "nbkg", 1e5, 0, 6e8);
      
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)] == 3000 or 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,1.0,100.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2300):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,130.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",5.0,0.0,130.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2100):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,120.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",6.0,0.0,120.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1700):
            sigma1 = r.RooRealVar("sigma1","sigma1",16.0,0.0,130.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",10.0,0.0,130.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",6.0,0.0,100.0)
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",5.0,3.0,100.0)
        n = r.RooRealVar("n1","n of both CB",0.85)
        alpha1 = r.RooRealVar("alpha1","alpha of first CB",3.5,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        if (PTSlicesD["PTlow"+str(i)] == 1700):
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 1200):
            frac = r.RooRealVar("frac","frac",0.3,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2200):
            frac = r.RooRealVar("frac","frac",0.05,0.,1.0)
        elif (PTSlicesD["PTlow"+str(i)] == 2600):
            frac = r.RooRealVar("frac","frac",0.4,0.,1.0)
        else:
            frac = r.RooRealVar("frac","frac",0.2,0.,1.0)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        if (PTSlicesD["PTlow"+str(i)] == 1700):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        else:
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
      
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
      
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHPlusMatched,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
      
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
      
        pad.Draw()
        pad.cd()
      
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
      
        #Data plotted on frame
        d_PlusM.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
      
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
      
      
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
            
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
      
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
      
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
      
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
      
        c.Modified()
        c.Update()
      
        c.SaveAs("PlotMassJ" + polarity + "PlusMatched" + str(PTSlicesD["PTlow"+str(i)])+"Syst.png");
        #SWeights calculated
        #sData = r.RooStats.SPlot("sData","An sPlot", d_PlusM, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "PlusMatched"+ str(PTSlicesD["PTlow"+str(i)])+"Syst.txt")
       
        """
        #max and min for signal and background fits applied
        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e9);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e9);
        #range and start point of fitting parameters defined
        #FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
        #frac = r.RooFormulaVar("frac","(1.0-FC)",r.RooArgList(FC));
      
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)]==1800):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,110.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,110.0);
        elif (PTSlicesD["PTlow"+str(i)]==2100):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,125.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,125.0);
        elif(PTSlicesD["PTlow"+str(i)] == 3900):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,2.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,1.0,100.0);
        elif(PTSlicesD["PTlow"+str(i)] == 2600):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,0.0,100.0); 
        elif(PTSlicesD["PTlow"+str(i)] == 3000):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,110.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8,0.0,110.0); 
        elif(PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",12.0,0.0,130.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,130.0);
        elif(PTSlicesD["PTlow"+str(i)] == 5400):
            sigma1 = r.RooRealVar("sigma1","sigma1",12.0,0.0,130.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",8.0,0.0,130.0);
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,2.0,100.0);
            sigma2 = r.RooRealVar("sigma2","sigma2",4.0,1.0,100.0);
        n = r.RooRealVar("n1","n of both CB",0.9)
        if (PTSlicesD["PTlow"+str(i)]==1200):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",0.525,0.,10.)
        elif (PTSlicesD["PTlow"+str(i)]==5400):
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",0.525,0.,10.)
        else:
            alpha1 = r.RooRealVar("alpha1","alpha of first CB",1.9,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        gauss1 = r.RooGaussian("#gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("#gauss2","gauss(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma1,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
      
        if (PTSlicesD["PTlow"+str(i)]==1700 or 2600 or 3400):
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        else:
            SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
      
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
      
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
      
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHMinus,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
      
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
      
        pad.Draw()
        pad.cd()
      
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
      
        #Data plotted on frame
        d_Minus.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
      
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
      
      
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
            
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
      
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
      
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
      
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
      
        c.Modified()
        c.Update()
      
        c.SaveAs("PlotMassJ" + polarity + "MinusFull" + str(PTSlicesD["PTlow"+str(i)]) + "Syst.png");
        #SWeights calculated
        #sData = r.RooStats.SPlot("sData","An sPlot", d_Minus, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "Minus" + str(PTSlicesD["PTlow"+str(i)]) + "Syst.txt")

        nsig =  r.RooRealVar ("nsig", "nsig", 1e5, 0, 1e9);
        nbkg =  r.RooRealVar ("nbkg", "nbkg", 1e5, 0, 6e9);
        #range for the mean and uncertainty defined with a start point to calculate fit
        mean1 = r.RooRealVar("mean1","mean1",3100,3000,3200.0);
        if (PTSlicesD["PTlow"+str(i)] == 1200):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,100.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,0.0,100.0)
        elif(PTSlicesD["PTlow"+str(i)] == 1700):
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,2.0,125.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,1.0,125.0)
        elif(PTSlicesD["PTlow"+str(i)] == 2300):
            sigma1 = r.RooRealVar("sigma1","sigma1",8.0,0.0,120.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",3.0,0.0,120.0)
        else:
            sigma1 = r.RooRealVar("sigma1","sigma1",10.0,0.0,130.0)
            sigma2 = r.RooRealVar("sigma2","sigma2",7.0,0.0,130.0)
        n = r.RooRealVar("n1","n of both CB",0.9)
        alpha1 = r.RooRealVar("alpha1","alpha of first CB",4,0.,10.)
        alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
        gauss1 = r.RooGaussian("gauss1","gauss(J_M, mean1, sigma1)",Jpsi_M,mean1,sigma1);
        gauss2 = r.RooGaussian("gauss2","gauss2(J_M, mean1, sigma2)",Jpsi_M,mean1,sigma2);
        CB1 = r.RooCBShape("CB1","CB1",Jpsi_M,mean1,sigma2,alpha1,n)
        CB2 = r.RooCBShape("CB2","CB2",Jpsi_M,mean1,sigma2,alpha2,n)
        frac = r.RooRealVar("frac","frac",0.1,0.,1.0)
      
        print("test");
        SignalmassPdf = r.RooAddPdf("SignalmassPdf","SignalmassPdf",r.RooArgList(gauss1,gauss2),r.RooArgList(frac))
        print("test");
      
      
        par = r.RooRealVar("par","par",0.0, -1.0, 1.0);
      
        Bkg_Jpsi_M = r.RooExponential("Bkg_Jpsi_M","Bkg_Jpsi_M", Jpsi_M, par);
      
        model = r.RooAddPdf("model", "model", r.RooArgList(SignalmassPdf,Bkg_Jpsi_M), r.RooArgList(nsig,nbkg))
        model.fitTo(dataHMinusMatched,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));
      
        #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
        c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
        pad = TPad("pad", "pad", 0., 0., 1., 1.)
        pad.SetTopMargin(0.95) #0.4
        pad.SetBottomMargin(0.3) #0.3
        pad.SetFillColor(0)
        pad.SetFillStyle(0)
      
        pad.Draw()
        pad.cd()
      
        #frame with title and axis defined
        Cmesframe = Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Events/[" + str(Bwidth) + " MeV]"));
      
        #Data plotted on frame
        d_MinusM.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));
      
        #plots fit, signal fit and background fit
        model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("SignalmassPdf"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
        model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_Jpsi_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
        #frame with title and axis define
        Cmesframe.GetXaxis().SetLabelOffset(0.02)
        Cmesframe.GetXaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetTitleOffset(1.2)
        Cmesframe.GetYaxis().SetLabelSize(0.035)
        Cmesframe.GetXaxis().SetLabelSize(0.035)
        Cmesframe.Draw();
      
      
        legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
        legend.SetFillStyle(0)
        legend.SetFillColor(0);
        legend.SetBorderSize(1);
            
        legend.SetTextSize(0.04);
        legend.SetTextFont(132);
        legend.AddEntry("CmyHist1", "Data", "p");
        legend.AddEntry("CmyCurve1", "Total", "l");
        legend.AddEntry("Sig", "Signal", "l");
        legend.AddEntry("Bkg", "Background(Exp)", "l");
        legend.Draw("same");
      
        print("test");
        pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
        pad1.SetTopMargin(0.8) #0.7
        pad1.SetBottomMargin(0.1) #0.15
        pad1.SetFillColor(0)
        pad1.SetFillStyle(0)
        pad1.Draw()
        pad1.cd()
      
        hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
        pad1.cd
        Cmesframe1 =Jpsi_M.frame(r.RooFit.Title(";{M(\mu^+\mu^-)}(MeV); Pull"))
        Cmesframe1.addPlotable(hpull,"P")
        Cmesframe1.SetMinimum(-7)
        Cmesframe1.SetMaximum(+7)
        Cmesframe1.GetYaxis().SetNdivisions(505)
        Cmesframe1.GetYaxis().SetTickLength(0.1)
        Cmesframe1.GetYaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelSize(0.035)
        Cmesframe1.GetXaxis().SetLabelOffset(0.02)
        Cmesframe1.GetXaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().SetTitleOffset(1.2)
        Cmesframe1.GetYaxis().CenterTitle(True)
        #frame2.GetXaxis().Set
        Cmesframe1.Draw("")
      
        mean1.setConstant()
        alpha1.setConstant()
        sigma1.setConstant()
        alpha2.setConstant()
        sigma2.setConstant()
        n.setConstant()
        par.setConstant()
      
        c.Modified()
        c.Update()
      
        c.SaveAs("PlotMassJ" + polarity + "MinusMatched" + str(PTSlicesD["PTlow"+str(i)]) + "Syst.png");
      
        #SWeights calculated
        #sData = r.RooStats.SPlot("sData","An sPlot", d_MinusM, model, r.RooArgList(nsig,nbkg))
        #sData.GetSDataSet().write("sweightsJ" + polarity + "MinusMatched" + str(PTSlicesD["PTlow"+str(i)])+"Syst.txt")
        """
def ObtainSWeightsDM(tree_1, polarity, low_mass, high_mass):
    import math

    #r.gROOT.SetBatch(True)
    #r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()

    input1=tree_1;

    print "input1", input1

    #gets a file and directory to open
    file1 = TFile.Open(input1);
    dir1 = file1.GetDirectory("")
    #gets the Decay Tree
    tin = dir1.Get("DecayTree")

   
    #Data from branch loaded
    print("D Fit Function Running")
    D_M = r.RooRealVar("D_M","Mass",low_mass,high_mass,"#it MeV/{c}^{2}")
    Bwidth = (high_mass-low_mass)/(D_M.getBins())
    data = r.RooDataSet("data","fit input dataset",tin, r.RooArgSet( D_M ))#, pass_all_cuts ), "pass_all_cuts>0")    
    print("D Data Successfully Read In")
    #max and min for signal and background fits applied
   
    dataH = r.RooDataHist("dataHist","fit input datasetHist",r.RooArgSet( D_M ), data)
    nsig =  r.RooRealVar ("nsig", "nsig", 20.E3, 0., 7.E4);
    nbkg =  r.RooRealVar ("nbkg", "nbkg", 350, 0., 6.E4);
   
    FC = r.RooRealVar("FC","FC",0.50,0.0,1.0);
    fracS = r.RooFormulaVar("fracS","(1.0-FC)",r.RooArgList(FC));
 
    nsig =  r.RooRealVar ("nsig", "nsig", 20.E5, 0., 10.E6)
    nbkg =  r.RooRealVar ("nbkg", "nbkg", 5.E5, 0., 10.E6)
 
    mean = r.RooRealVar("mean","mean of both CB",1869.65,1840.0,1900.0)
    sigma1 = r.RooRealVar("sigma1","sigma1",10.0,3.0,70.0)
    sigma2 = r.RooRealVar("sigma2","sigma2",5.0,3.0,70.0)
    n = r.RooRealVar("n1","n of both CB",0.9)
    alpha1 = r.RooRealVar("alpha1","alpha of first CB",6,0.,10)
    alpha2 = r.RooRealVar("alpha2","alpha of second CB",1.9,0.,10.)
    CB1 = r.RooCBShape("CB1","CB1",D_M,mean,sigma1,alpha1,n)
    CB2 = r.RooCBShape("CB2","CB2",D_M,mean,sigma2,alpha2,n)
    gauss1 = r.RooGaussian("gauss1","gauss(J_M, mean1, sigma1)",D_M,mean,sigma1);
    gauss2 = r.RooGaussian("gauss2","gauss2(J_M, mean1, sigma2)",D_M,mean,sigma2);
    BW1 = r.RooBreitWigner("BW1","BW1(D_M, mean1, sigma1)",D_M,mean,sigma1);
    BW2 = r.RooBreitWigner("BW2","BW2(D_M, mean1, sigma2)",D_M,mean,sigma2);
    frac = r.RooRealVar("frac","frac",0.1,0.,1.0);

    signal = r.RooAddPdf("signal","signal",r.RooArgList(CB1,CB2),r.RooArgList(frac))
 
    par = r.RooRealVar("par","par",0.1, -1.0, 1.0);

    Bkg_D_M = r.RooExponential("Bkg_D_M","Bkg_D_M", D_M, par)

    model = r.RooAddPdf("model", "model", r.RooArgList(signal,Bkg_D_M), r.RooArgList(nsig,nbkg))
    model.fitTo(dataH,r.RooFit.Extended(r.kTRUE),r.RooFit.Save(r.kTRUE),r.RooFit.NumCPU(8,r.kTRUE),r.RooFit.PrintLevel(1));

    #creates a canvas called test and sets the directory to this canvas (any fits and data are plotted on this canvas)
    c=r.TCanvas("Mass2", "Mass2", 0, 650, 850, 500);
    c.cd();

    pad = TPad("pad", "pad", 0., 0., 1., 1.)
    pad.SetTopMargin(0.95) #0.4
    pad.SetBottomMargin(0.3) #0.3
    pad.SetFillColor(0)
    pad.SetFillStyle(0)

    pad.Draw()
    pad.cd()

    #frame with title and axis defined
    Cmesframe = D_M.frame(r.RooFit.Title("; M(MeV); Events/[" + str(Bwidth) + " MeV]"));

    #Data plotted on frame
    data.plotOn(Cmesframe, r.RooFit.Name("CmyHist1"));

    #plots fit, signal fit and background fit
    model.plotOn(Cmesframe, r.RooFit.Name("CmyCurve1"),r.RooFit.LineColor(4));
    model.plotOn(Cmesframe, r.RooFit.Name("Sig"),r.RooFit.Components("signal"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(4));
    model.plotOn(Cmesframe, r.RooFit.Name("Bkg"),r.RooFit.Components("Bkg_D_M"),r.RooFit.LineStyle(r.kDashed),r.RooFit.LineColor(2));
   #frame with title and axis define
    Cmesframe.GetXaxis().SetLabelOffset(0.02)
    Cmesframe.GetXaxis().SetTitleOffset(1.2)
    Cmesframe.GetYaxis().SetTitleOffset(1.2)
    Cmesframe.GetYaxis().SetLabelSize(0.035)
    Cmesframe.GetXaxis().SetLabelSize(0.035)
    Cmesframe.Draw();


    legend = r.TLegend(0.67, 0.71, 0.89, 0.88);
    legend.SetFillStyle(0)
    legend.SetFillColor(0);
    legend.SetBorderSize(1);
         
    legend.SetTextSize(0.04);
    legend.SetTextFont(132);
    legend.AddEntry("CmyHist1", "Data", "p");
    legend.AddEntry("CmyCurve1", "Total", "l");
    legend.AddEntry("Sig", "Signal", "l");
    legend.AddEntry("Bkg", "Background(Exp)", "l");
    legend.Draw("same");

    print("test");
    pad1 = TPad("pad1", "pad1", 0., 0., 1., 1.)
    pad1.SetTopMargin(0.8) #0.7
    pad1.SetBottomMargin(0.1) #0.15
    pad1.SetFillColor(0)
    pad1.SetFillStyle(0)
    pad1.Draw()
    pad1.cd()

    hpull = Cmesframe.pullHist("CmyHist1", "CmyCurve1")
    pad1.cd
    Cmesframe1 =D_M.frame(r.RooFit.Title(";M(MeV); Pull"))
    Cmesframe1.addPlotable(hpull,"P")
    Cmesframe1.SetMinimum(-7)
    Cmesframe1.SetMaximum(+7)
    Cmesframe1.GetYaxis().SetNdivisions(505)
    Cmesframe1.GetYaxis().SetTickLength(0.1)
    Cmesframe1.GetYaxis().SetLabelSize(0.035)
    Cmesframe1.GetXaxis().SetLabelSize(0.035)
    Cmesframe1.GetXaxis().SetLabelOffset(0.02)
    Cmesframe1.GetXaxis().SetTitleOffset(1.2)
    Cmesframe1.GetYaxis().SetTitleOffset(1.2)
    Cmesframe1.GetYaxis().CenterTitle(True)
    #frame2.GetXaxis().Set
    Cmesframe1.Draw("")

    mean.setConstant()
    sigma1.setConstant()
    sigma2.setConstant()
    n.setConstant();
    alpha1.setConstant();
    alpha2.setConstant();
    par.setConstant()
   
    c.Update()

    c.SaveAs("PlotMassD" + polarity + ".png");
   
    #SWeights calculated
    #sData = r.RooStats.SPlot("sData","An sPlot", data, model, r.RooArgList(nsig,nbkg))
   
    #sData.GetSDataSet().write("sweightsD" + polarity + ".txt")

    print("D sWeights Written")
   
   

def ObtainTList(polarity, direction):
    import math
    list1 = TChain("TupleLong"+ direction+"/DecayTree")
    for (i) in range(0, 2000):
        print(i);
        if (polarity == "Up"):
            print("UP")
            if (i < 9):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066676_0000000" + str(i+1) + "_1.trkcalib.root"
            elif (8 < i < 99):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066676_000000" + str(i+1) + "_1.trkcalib.root"
       
            elif (98 < i < 999):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066676_00000" + str(i+1) + "_1.trkcalib.root"

            else:
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066676_0000" + str(i+1) + "_1.trkcalib.root"

        else:
            print("DOWN")
            if (i < 9):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066675_0000000" + str(i+1) + "_1.trkcalib.root"
            elif (8 < i < 99):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066675_000000" + str(i+1) + "_1.trkcalib.root"
       
            elif (98 < i < 999):
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066675_00000" + str(i+1) + "_1.trkcalib.root"

            else:
                input1 = "/eos/lhcb/wg/TrackingAlignment/WGP_Oct2017/Data_2016_25ns_Mag" + polarity + "/00066675_0000" + str(i+1) + "_1.trkcalib.root"
       
        if os.path.isfile(input1):
           
            list1.Add(input1)

        else:
            continue
    return list1

    print ("All events processed.")


#writing TList so they do not have to be created every time
def writeTList(JPsiUpPlus, JPsiUpMinus, JPsiDownPlus, JPsiDownMinus, output_file):
    fout = r.TFile(output_file,'RECREATE');
    fout.cd();
    JPsiUpPlus.Write("JPsiUpPlus", 1)
    JPsiUpMinus.Write("JPsiUpMinus", 1)
    JPsiDownPlus.Write("JPsiDownPlus", 1)
    JPsiDownMinus.Write("JPsiDownMinus", 1)
