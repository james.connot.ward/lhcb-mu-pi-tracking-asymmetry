from ROOT import TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, RooFormulaVar, RooDataSet, RooDataHist, RooCategory, RooKeysPdf, TLorentzVector, TList, gROOT, gDirectory, TAxis, RooPlot, RooHist, RooPlotable, RooCurve,TGLHistPainter, TPad, TGraph, gStyle
import ROOT as r

import numpy as np
import os
import json
from array import array

def plot_SignalAll(DHists, JPlusHists, output_file, home, polarity, sliceNumber, direction):
    import math
    PTlow = 0;
    PThigh = 10;
    PHIlow = -3.2;
    PHIhigh = 3.2;
    ETAlow = 1.25;
    ETAhigh = 5;
   
    gStyle.SetPadLeftMargin(0.12);
    gStyle.SetPadRightMargin(0.05)
    #bin widths for axis labels
    PHIwidth = (PHIhigh-PHIlow)/100;
    ETAwidth = 0.05;
    PTwidth  = (PThigh-PTlow)/100;
    print(PTwidth)
    r.gStyle.SetOptStat(0)
   
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle() 
    #files containing data opened
    file1 = TFile.Open(JPlusHists)      
    file2 = TFile.Open(DHists);
    #histograms defined for signal and muon kinematics 
    sigMu_PT = TH1F();  
    sigPion_PT = TH1F();
    sigMu_ETA = TH1F();  
    sigPion_ETA = TH1F();
    sigMu_PHI = TH1F();  
    sigPion_PHI = TH1F();
    J_PT = TH1F();
    J_ETA = TH1F();  
    J_PHI = TH1F();

    #histograms loaded from file locations
    file2.GetObject("MuETA" + polarity, sigMu_ETA)
    file2.GetObject("PiETA" + polarity, sigPion_ETA)  
    file2.GetObject("MuPT" + polarity, sigMu_PT)
    file2.GetObject("PiPT" + polarity, sigPion_PT)  
    file2.GetObject("MuPHI" + polarity, sigMu_PHI)
    file2.GetObject("PiPHI" + polarity, sigPion_PHI)
    file1.GetObject("JPsiKinPTPlot"+ polarity + direction, J_PT)
    file1.GetObject("JPsiKinETAPlot"+ polarity + direction, J_ETA)
    file1.GetObject("JPsiKinPHIPlot"+ polarity + direction, J_PHI)

    #PT distibutions converted to GeV
    sigPiPT = TH1F("sigPiPT", "sigPiPT", 100, 0, 10)
    sigMuPT = TH1F("sigMuPT", "sigMuPT", 100, 0, 10)
    JPT = TH1F("JPT", "JPT", 100, 0 ,10)
    for i in xrange(100):
        sigPiPT.SetBinContent(i, sigPion_PT.GetBinContent(i));
        sigMuPT.SetBinContent(i, sigMu_PT.GetBinContent(i)); 
        JPT.SetBinContent(i, J_PT.GetBinContent(i)); 
        
    #output file created
    fout = r.TFile(output_file,'RECREATE')      
    
    #title set to blanck
    sigMu_PT = sigMuPT.Clone();
    sigPion_PT = sigPiPT.Clone();
    sigMu_ETA.SetTitle("");
    sigPion_ETA.SetTitle("");
    sigMuPT.SetTitle("");
    sigPiPT.SetTitle("");
    sigMu_PHI.SetTitle("");
    sigPion_PHI.SetTitle("");
    JPT.SetTitle("");  
    J_PHI.SetTitle("");  
    J_ETA.SetTitle("");  
    

    #creates a canvas and sets the directory to this canvas (data are plotted on this canvas)
    c=r.TCanvas("Mass1", "Mass1", 0, 650, 850, 500);
    #pad declared on canvas
    pad = TPad("pad", "pad", 0.02, 0.03, 1., 1.)
    #pad.SetTopMargin(1) #0.4
    pad.SetBottomMargin(0.12) #0.3
    pad.SetFillColor(0)
    pad.SetFillStyle(0)
    
    pad.Draw()
    pad.cd()

    #all transverse momentum distributions plotted overlaid
    sigPiPT.SetLineWidth(2)
    sigMuPT.SetLineWidth(2)
    sigPiPT.Scale(sigMuPT.Integral("width")/sigPiPT.Integral("width"))
    sigPiPT.SetMarkerColor(2);
    sigPiPT.SetLineColor(2);
    (sigPiPT.GetXaxis()).SetTitle("p_{T}[GeV]");
    (sigPiPT.GetYaxis()).SetTitle("Events/[0.1 GeV]");
    sigPiPT.GetXaxis().SetTitleOffset(1.1);
    sigPiPT.GetXaxis().SetLabelSize(0.058);
    sigPiPT.GetYaxis().SetLabelSize(0.058);
    sigPiPT.GetXaxis().SetTitleSize(0.058);
    sigPiPT.GetYaxis().SetTitleSize(0.058);
    sigPiPT.Draw("hist");
    sigPiPT.SetMaximum(180000)
    c.Update();
   
    sigMuPT.SetMarkerColor(4);
    sigMuPT.SetLineColor(4);
    sigMuPT.Draw("same hist");
    c.Update();
    #JPT.Scale(sigMuPT.Integral("width")/JPT.Integral("width"))
    JPT.SetMarkerColor(1);
    JPT.SetLineColor(1);
    JPT.SetLineWidth(2);
    JPT.Draw("same hist");    
    c.Update();
    c.SetName("SigMuPi_KinematicsPT")

    leg = r.TLegend(0.82,0.65,0.93,0.875)    
    leg.SetFillColor(0)    
    leg.SetBorderSize(0)    
    leg.SetTextFont(132)
    leg.SetTextSize(0.07)
    leg.AddEntry(sigPiPT, "\pi_{sig}") 
    leg.AddEntry(sigMuPT, "\mu_{sig}")
    leg.AddEntry(JPT, "\mu_{tap}")
    leg.Draw()


    leg.Draw()    
    c.Update()

    c.Write();
   
    c.SaveAs(home +"/MuPiPT" + polarity + direction +".png")
    
    #canvas and pad for ETA disributions
    f=r.TCanvas("f", "f", 0, 650, 850, 500);
    pad1 = TPad("pad", "pad", 0.02, 0.03, 1., 1.)
    #pad1.SetTopMargin(1) #0.4
    pad1.SetBottomMargin(0.12) #0.3
    pad1.SetFillColor(0)
    pad1.SetFillStyle(0)
    
    pad1.Draw()
    pad1.cd()
    #all ETA distributions plotted overlaid
    sigPion_ETA.SetLineWidth(2)
    sigMu_ETA.SetLineWidth(2)
    sigPion_ETA.Scale(sigMu_ETA.Integral("width")/sigPion_ETA.Integral("width"))
    sigPion_ETA.SetMarkerColor(2);
    sigPion_ETA.SetLineColor(2);
    (sigPion_ETA.GetXaxis()).SetTitle("\eta");
    (sigPion_ETA.GetYaxis()).SetTitle("Events/[0.05]");
    sigPion_ETA.SetMaximum(125000)
    sigPion_ETA.GetXaxis().SetLabelSize(0.058);
    sigPion_ETA.GetYaxis().SetLabelSize(0.058);
    sigPion_ETA.GetXaxis().SetTitleSize(0.058);
    sigPion_ETA.GetYaxis().SetTitleSize(0.058);
    sigPion_ETA.GetXaxis().SetRange(25,100);
    sigPion_ETA.Draw("hist");
    f.Update();
   
    sigMu_ETA.SetMarkerColor(4);
    sigMu_ETA.SetLineColor(4);
    sigMu_ETA.Draw("same hist");
    f.Update();
    J_ETA.SetMarkerColor(1);
    J_ETA.SetLineColor(1);
    J_ETA.SetLineWidth(2);
    J_ETA.Draw("same hist");
    f.Update();
    f.SetName("SigMuPi_KinematicsETA")
   
    leg = r.TLegend(0.82,0.65,0.93,0.875)    
    leg.SetFillColor(0)    
    leg.SetBorderSize(0)    
    leg.SetTextFont(132)
    leg.SetTextSize(0.07)
    leg.AddEntry(sigPion_ETA, "\pi_{sig}")  
    leg.AddEntry(sigMu_ETA, "\mu_{sig}")
    leg.AddEntry(J_ETA, "\mu_{tap}")
    leg.Draw()
   
    f.Update()

    f.Write();
    f.SaveAs(home +"/MuPiETA" + polarity + direction +".png")

    #canvas and pad for PHI distributions
    g=r.TCanvas("g", "g", 0, 650, 850, 500)
    g.cd();

    pad2 = TPad("pad", "pad", 0.02, 0.03, 1., 1.)
    pad2.SetTopMargin(1) #0.4
    pad2.SetBottomMargin(0.12) #0.3
    pad2.SetFillColor(0)
    pad2.SetFillStyle(0)
    
    pad2.Draw()
    pad2.cd()
    
    #all PHI distributions plotted overlaid
    sigPion_PHI.SetLineWidth(2)
    sigMu_PHI.SetLineWidth(2)
    sigPion_PHI.Scale(sigMu_PHI.Integral("width")/sigPion_PHI.Integral("width"))
    sigPion_PHI.SetMarkerColor(2);
    sigPion_PHI.SetLineColor(2);
    (sigPion_PHI.GetXaxis()).SetTitle("\phi");
    (sigPion_PHI.GetYaxis()).SetTitle("Events/[" + str(PHIwidth) + "]");
    sigPion_PHI.SetMaximum(60000)
    (sigPion_PHI.GetYaxis()).SetTitleOffset(1.2)
    sigPion_PHI.GetXaxis().SetLabelSize(0.058);
    sigPion_PHI.GetYaxis().SetLabelSize(0.058);
    sigPion_PHI.GetXaxis().SetTitleSize(0.058);
    sigPion_PHI.GetYaxis().SetTitleSize(0.058);
    sigPion_PHI.Draw("hist");
    g.Update();
   
    sigMu_PHI.SetMarkerColor(4);
    sigMu_PHI.SetLineColor(4);
    sigMu_PHI.Draw("same hist");
    g.Update();
    
    J_PHI.SetMarkerColor(1);
    J_PHI.SetLineColor(1);
    J_PHI.SetLineWidth(2);
    J_PHI.Draw("same hist");
    g.Update();
    
    g.SetName("SigMuPi_KinematicsPHI")
    
    leg = r.TLegend(0.82,0.65,0.93,0.875)    
    leg.SetFillColor(0)    
    leg.SetBorderSize(0)    
    leg.SetTextFont(132)
    leg.SetTextSize(0.07)
    leg.AddEntry(sigPion_PHI, "\pi_{sig}")  
    leg.AddEntry(sigMu_PHI, "\mu_{sig}")
    leg.AddEntry(J_PHI, "\mu_{tap}")
    leg.Draw()

    leg.Draw()
   
    g.Update()

    g.Write();
    print("complete");   
    g.SaveAs(home +"/MuPiPHI" + polarity + direction + ".png")
    print("complete");