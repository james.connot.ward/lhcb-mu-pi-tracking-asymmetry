import sys
import numpy as np

###import functions from relevant files
from fitting_variablesPT import storeSWeights
from fitting_variablesPT import storeSWeightsSystematics
from fitting_variablesPTDown import storeSWeightsDown
from fitting_variablesPT import GetDataSet
from fitting_variablesPT import MergeTrees
from fitting_variablesPT import ObtainSWeightsDM

from fillHistPT import FillJPsiAdapt
from fillHistPT import FillDHistAdapt

from KinematicsPlot import plot_SignalAll
from plot1dhist import plot_1Dhist
from AsymmCalc import twoDHists
from AsymmCalc3D import threeDHists


#Trees storing signal data
tree1 = "/eos/lhcb/user/l/liyu/asld_analysis/writeTuple/Data2016/RunII_Data2016_MagUp.root"
tree2 = "/eos/lhcb/user/l/liyu/asld_analysis/writeTuple/Data2016/RunII_Data2016_MagDown.root"
home =  "/eos/lhcb/user/j/jward/LHCb2016Data"

###adaptively binned histograms for D data with magnet polarity aligned upwards
output_WeightedDUpAdaptive8 = "output_WeightedDUpAdaptive8.root"
output_WeightedDUpAdaptive10 = "output_WeightedDUpAdaptive10.root"
###adaptively binned histograms for D data with magnet polarity aligned downwards
output_WeightedDDownAdaptive8 = "output_WeightedDDownAdaptive8.root"
output_WeightedDDownAdaptive10 = "output_WeightedDDownAdaptive10.root"

####adaptively binned histograms for plus and minus probe tracks with magnet polarity aligned upwards
FilledHistsPTSlicedFitsPlusUP = "FilledHistsPTSlicedFitsPlusUP.root"
FilledHistsPTSlicedFitsMinusUP = "FilledHistsPTSlicedFitsMinusUP.root"
####adaptively binned histograms for plus and minus probe tracks with magnet polarity aligned Downwards
FilledHistsPTSlicedFitsPlusDown = "FilledHistsPTSlicedFitsPlusDown.root"
FilledHistsPTSlicedFitsMinusDown = "FilledHistsPTSlicedFitsMinusDown.root"
####systematic check adaptively binned histograms for plus and minus probe tracks with magnet polarity aligned upwards
FilledHistsPTSlicedFitsPlusUPSyst = "FilledHistsPTSlicedFitsPlusUPSyst.root"
FilledHistsPTSlicedFitsMinusUPSyst = "FilledHistsPTSlicedFitsMinusUPSyst.root"
####systematic check adaptively binned histograms for plus and minus probe tracks with magnet polarity aligned Downwards
FilledHistsPTSlicedFitsPlusDownSyst = "FilledHistsPTSlicedFitsPlusDownSyst.root"
FilledHistsPTSlicedFitsMinusDownSyst = "FilledHistsPTSlicedFitsMinusDownSyst.root"

###file to store 1D asymm dependence histograms 
output_1D ="OneDHists.root"
###file to store signal kinematic distribution histograms
output_KinUp = "kinematicsUp.root"
#outputs for twoD hists
output_2DSlicedHists = "output_2DSlicedHists"
output_2DSlicedHistsSR = "output_2DSlicedHistsSR.root"
###datasets in PT slices for PT fits
UpDataSets2016PT = "UpDataSets2016PT.root"
UpDataSets2016PTSyst = "UpDataSets2016PTSyst.root"
DownDataSets2016PT = "DownDataSets2016PT.root"

#merged control tag and probe data
tree3 = "/eos/lhcb/user/j/jward/LHCb2016Data/mergedtrkcalibUpPlus.root"
tree4 = "/eos/lhcb/user/j/jward/LHCb2016Data/mergedtrkcalibUpMinus.root"

#merged control tag and probe data down
tree5 = "/eos/lhcb/user/j/jwilder/LHCb2016Data/semester2/finalData/mergedtrkcalibDownPlus.root"
tree6 = "/eos/lhcb/user/j/jwilder/LHCb2016Data/semester2/finalData/mergedtrkcalibDownMinus.root"

LMJ=2800
HMJ=3400

LMD= 1800
HMD= 1940

####function to merge trkcalib files into one tree to speed up later process
####NOTE CAN RUN AT THE SAME TIME
#MergeTrees(tree3, "Up", "Plus", LMJ, HMJ)
#MergeTrees(tree4, "Up", "Minus", LMJ, HMJ)
#MergeTrees(tree5, "Down", "Plus", LMJ, HMJ)
#MergeTrees(tree6, "Down", "Minus", LMJ, HMJ)

####Function called to obtain sWeights from Yunlong's Signal Event trees
####Tree file location defined using variable tree1 for magUp, tree2 for MagDown,sWeights are saved in text file
####NOTE CAN RUN AT THE SAME TIME
#ObtainSWeightsDM(tree1, "Up", LMD, HMD);
#ObtainSWeightsDM(tree2, "Down", LMD, HMD);

####Function to Fill D Histograms and create Json files with ranges for adaptive binning
####NOTE CAN RUN AT THE SAME TIME
#FillDHistAdapt(output_WeightedDUpAdaptive8, LMD, HMD, tree1, "Up", 8, False)
#FillDHistAdapt(output_WeightedDDownAdaptive8, LMD, HMD, tree2, "Down", 8, False)
#FillDHistAdapt(output_WeightedDUpAdaptive10, LMD, HMD, tree1, "Up", 10, True)
#FillDHistAdapt(output_WeightedDDownAdaptive10, LMD, HMD, tree2, "Down", 10, True)

####function Called to obtain sWeights for plus and minus charged long track muons
####NOTE CAN RUN AT THE SAME TIME in different terminals
#calculating j/psi sweights and storing in text file
#GetDataSet(tree3,tree4,"Up",LMJ,HMJ, UpDataSets2016PT, 8, False)
#GetDataSet(tree3,tree4,"Up",LMJ,HMJ, UpDataSets2016PTSyst, 10, True)
#GetDataSet(tree5,tree6,"Down",LMJ,HMJ, DownDataSets2016PT, 8)
#GetDataSet(tree5,tree6,"Down",LMJ,HMJ, DownDataSets2016PTSyst, 10, True)

####function Called to obtain sWeights for plus and minus charged long track muons
####NOTE CAN RUN AT THE SAME TIME in different terminals
#storeSWeights("Up", LMJ, HMJ, UpDataSets2016PT, 8);
#storeSWeightsSystematics("Up", LMJ, HMJ, UpDataSets2016PTSyst, 10)
#storeSWeightsDown("Down", LMJ, HMJ, DownDataSets2016PT, 8)
#storeSWeightsSystematics("Down", LMJ, HMJ, DownDataSets2016PTSyst, 10)

####Function to Fill J/PSi Histograms for Plus and Minus probe tracks
####NOTE CAN RUN AT THE SAME TIME in different terminals
#FillJPsiAdapt(FilledHistsPTSlicedFitsPlusUP, tree3, LMJ, HMJ, "Up", "Plus", "Probe", 8, "original")
#FillJPsiAdapt(FilledHistsPTSlicedFitsMinusUP, tree4, LMJ, HMJ, "Up", "Minus", "Probe", 8, "original")
#FillJPsiAdapt(FilledHistsPTSlicedFitsPlusUPSyst, tree3, LMJ, HMJ, "Up", "Plus", "Probe", 10, True)
#FillJPsiAdapt(FilledHistsPTSlicedFitsMinusUPSyst, tree4, LMJ, HMJ, "Up", "Minus", "Probe", 10, True)


####plot1DHist function plots the 1D asymmetries
#plot_1Dhist(FilledHistsPTSlicedFitsPlusUP, FilledHistsPTSlicedFitsMinusUP, output_1D, home, "Up", 8)

####plot overlaid Kinematics of signal muon and control minus probe kinematics
#plot_SignalAll(output_WeightedDUpAdaptive8, FilledHistsPTSlicedFitsPlusUP,output_KinUp, home, "Up", 8, "Plus")

####function called to obtain asymmetries for 2D slices using all methods (function called for original and systematic check results)
#twoDHists(output_WeightedDUpAdaptive8,FilledHistsPTSlicedFitsPlusUP, FilledHistsPTSlicedFitsMinusUP, output_WeightedDUpAdaptive10,FilledHistsPTSlicedFitsPlusUPSyst, FilledHistsPTSlicedFitsMinusUPSyst,output_2DSlicedHists,  output_2DSlicedHistsSR,"Up", home, 8, 10)
#twoDHists(output_WeightedDDownAdaptive8,FilledHistsPTSlicedFitsPlusDown, FilledHistsPTSlicedFitsMinusDown, output_WeightedDDownAdaptive10,FilledHistsPTSlicedFitsPlusDownSyst, FilledHistsPTSlicedFitsMinusDownSyst,output_2DSlicedHists,  output_2DSlicedHistsSR,"Down", home, 8, 10)
