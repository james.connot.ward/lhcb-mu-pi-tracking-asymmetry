from ROOT import TChain, TFile, TTree, TBranch, TTreeReader, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, RooFormulaVar, RooDataSet, RooDataHist, RooCategory, RooKeysPdf, TLorentzVector, TList, gROOT, gDirectory, TAxis, RooPlot, RooHist, RooPlotable, RooCurve,TGLHistPainter, TPad, TGraph, gStyle
import ROOT as r

import pandas as pd
import numpy as np
from array import array
import json
import os
      
    
def twoDHists(DHists, JPlusHists, JMinusHists, DHistsSyst, JPlusHistsSyst, JMinusHistsSyst, output_Hists, output_HistsSR, polarity, home, sliceNumber, sliceNumberSyst):
    #arrays declared to save outputs in a csv
    PTlowL=[]
    efficiencyPlusL=[]
    efficiencyMinusL=[]
    errorEPlusL=[]
    errorEMinusL=[]
    AsymmJL=[]
    errorAsymmL=[]
    GlobalAsymmL=[]
    GlobalAsymmEL=[]
    MuAsymmL=[]
    MuAsymmEL=[]
    PiAsymmL=[]
    PiAsymmEL=[]
    CorrErr=[]
    
    PTlowLS=[]
    efficiencyPlusLS=[]
    efficiencyMinusLS=[]
    errorEPlusLS=[]
    errorEMinusLS=[]
    AsymmJLS=[]
    errorAsymmLS=[]
    GlobalAsymmLS=[]
    GlobalAsymmELS=[]
    MuAsymmLS=[]
    MuAsymmELS=[]
    PiAsymmLS=[]
    PiAsymmELS=[]
    CorrErrS=[]
    fout = r.TFile(output_Hists,'RECREATE');
    r.gStyle.SetOptStat(0)
   
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()
    
    #open Json file with bin limits
    PTSlicesD = json.load( open( "PTSlicesD"+polarity+".json" ) )
    PTSlicesDSyst = json.load( open( "PTSlicesD"+polarity+"Syst.json" ) )
    
    def TwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTlow, home, sliceNumber, systematic):
        ###open file with histograms filled with sWeights
        file1 = TFile.Open(DHists)
        file2 = TFile.Open(JPlusHists)
        file3 = TFile.Open(JMinusHists)
        ###Get histograms to be used from files 
        DMu3DName = TH3F();
        file1.GetObject("SignalKinMu"+polarity, DMu3DName);       
        
        DPi3DName = TH3F();
        file1.GetObject("SignalKinPion"+polarity, DPi3DName);               
        
        DMuName = TH2F();
        file1.GetObject("DMu2D"+polarity+str(PTlow), DMuName);
        
        DMuQuadName = TH2F();
        file1.GetObject("DMuQUAD2D"+polarity+str(PTlow), DMuQuadName);        
       
        DPiQuadName = TH2F();
        file1.GetObject("DPiQUAD2D"+polarity+str(PTlow), DPiQuadName);   
        
        DPiName = TH2F();
        file1.GetObject("DPi2D"+polarity+str(PTlow), DPiName);
        ###NOTE from this point onwards variables are labelled by PROBE track charge
        ###INSTEAD OF muon tag track charge
        JMinusName = TH2F();
        file2.GetObject("JPsiKin"+ polarity + "Plus"+ str(PTlow),JMinusName);
       
        JPlusName = TH2F();
        file3.GetObject("JPsiKin"+ polarity + "Minus" + str(PTlow), JPlusName);

        JMinusAssocName = TH2F();
        file2.GetObject("JPsiKinAssoc"+ polarity + "Plus"+ str(PTlow),JMinusAssocName);
       
        JPlusAssocName = TH2F();
        file3.GetObject("JPsiKinAssoc"+ polarity + "Minus" + str(PTlow),JPlusAssocName);

        fout.cd();
        
        #normalise kinematic distributions of signal muon and pion
        SigKinMu = DMuName.Clone();
        SigKinMu.Multiply(DMuName);
        SigKinMu.Divide(DMuQuadName)

        SigKinPi = DPiName.Clone();
        SigKinPi.Multiply(DPiName);
        SigKinPi.Divide(DPiQuadName);

        ###total number of signal muons and pions in the PT slice
        NSIGMu = 0;
        NSIGPi = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                    NSIGMu = NSIGMu + DMuName.GetBinContent(i,j);
                    NSIGPi = NSIGPi + DPiName.GetBinContent(i,j);
        ###histograms for full probe tracks
        JPsiKinMinus = JMinusName.Clone();
        JPsiKinPlus = JPlusName.Clone();
        """
        ###2D histograms of full tag and probe muon distributions in PT slice
        fp=r.TCanvas("fp", "fp",910,600)
        fp.cd();
        
        JPsiKinPlus.Draw("COLZ1")
        
        fp.Update()
        paletteFP = JPsiKinPlus.GetListOfFunctions().FindObject("palette")
        paletteFP.SetX1NDC(0.91);
        paletteFP.SetX2NDC(0.935);
        paletteFP.SetY1NDC(0.1);
        paletteFP.SetY2NDC(0.9);
        paletteFP.SetLabelOffset(-0.03)
        fp.Update()
       
        fp.Write();
   
        fp.SaveAs(home + "/JPsiPlus"+str(PTlow)+ ".png")
        
        fm=r.TCanvas("fm", "fm",910,600)
        fm.cd();
        JPsiKinMinus.Draw("COLZ1")

        fm.Update()
        paletteFM = JPsiKinMinus.GetListOfFunctions().FindObject("palette")
        paletteFM.SetX1NDC(0.91);
        paletteFM.SetX2NDC(0.935);
        paletteFM.SetY1NDC(0.1);
        paletteFM.SetY2NDC(0.9);
        paletteFM.SetLabelOffset(-0.03)
        fm.Update()
       
        fm.Write();
   
        fm.SaveAs(home + "/JPsiMinus"+str(PTlow)+ ".png")
        """
        ###histograms for matched probe tracks
        JPsiKinMinusAssoc = JMinusAssocName.Clone();
        JPsiKinPlusAssoc =  JPlusAssocName.Clone();
        """
        ###2D histograms of matched tag and probe muon distributions in PT slices
        mp=r.TCanvas("mp", "mp",910,600)
        mp.cd();
        JPsiKinPlusAssoc.Draw("COLZ1")
        
        mp.Update()
        paletteMP = JPsiKinPlusAssoc.GetListOfFunctions().FindObject("palette")
        paletteMP.SetX1NDC(0.91);
        paletteMP.SetX2NDC(0.935);
        paletteMP.SetY1NDC(0.1);
        paletteMP.SetY2NDC(0.9);
        paletteMP.SetLabelOffset(-0.03)
        mp.Update()
       
        mp.Write();
   
        mp.SaveAs(home + "/JPsiPlusAssoc"+str(PTlow)+ ".png")
        
        mm=r.TCanvas("mm", "mm",910,600)
        mm.cd();
        JPsiKinMinusAssoc.Draw("COLZ1")

        mm.Update()
        paletteMM = JPsiKinMinusAssoc.GetListOfFunctions().FindObject("palette")
        paletteMM.SetX1NDC(0.91);
        paletteMM.SetX2NDC(0.935);
        paletteMM.SetY1NDC(0.1);
        paletteMM.SetY2NDC(0.9);
        paletteMM.SetLabelOffset(-0.03)
        mm.Update()
       
        mm.Write();
   
        mm.SaveAs(home + "/JPsiMinusAssoc"+str(PTlow)+ ".png")
        """
        ###calculate total number of muon tag and probe events in the slice        
        TotSigMu = 0;
        TotSigPi = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                    TotSigMu = TotSigMu + SigKinMu.GetBinContent(i,j);
                    TotSigPi = TotSigPi + SigKinPi.GetBinContent(i,j);
        ###histograms storing total number of tag and probe events; 1; 100
        ###note values are same in every bin
        TotSigKinMu = SigKinMu.Clone();
        TotSigKinPi = SigKinPi.Clone();
        OneHist = SigKinPi.Clone();
        OneHist.SetName("one");
        OneHundredHist = SigKinPi.Clone();
        OneHundredHist.SetName("oneHundred");
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                    TotSigKinMu.SetBinContent(i,j, TotSigMu);
                    TotSigKinPi.SetBinContent(i,j, TotSigPi);
                    OneHist.SetBinContent(i,j, 1);
                    OneHundredHist.SetBinContent(i,j, 100);
        ###signal muon and pion distributions calclated
        SigKinMuProb = SigKinMu.Clone();
        SigKinMuProb.Divide(TotSigKinMu);
        SigKinMuProb.SetName("SigKinMuProb");

        SigKinPiProb = SigKinPi.Clone();
        SigKinPiProb.Divide(TotSigKinPi);
        SigKinPiProb.SetName("SigKinPiProb");

        ###signal muon and pion distibutions plotted
        """
        SigKinMuProb.SetTitle("")
        (SigKinMuProb.GetXaxis()).SetTitleOffset(1.1)
        (SigKinMuProb.GetYaxis()).SetTitleOffset(0.8)
        (SigKinMuProb.GetZaxis()).SetTitleOffset(1.2)
        (SigKinMuProb.GetXaxis()).SetTitle("\phi");
        (SigKinMuProb.GetYaxis()).SetTitle("\eta");
        (SigKinMuProb.GetXaxis()).SetTitleSize(0.05);
        (SigKinMuProb.GetYaxis()).SetTitleSize(0.05);
        (SigKinMuProb.GetXaxis()).SetLabelSize(0.05);
        (SigKinMuProb.GetYaxis()).SetLabelSize(0.05);
        (SigKinMuProb.GetZaxis()).SetTitleSize(0.05);
        (SigKinMuProb.GetZaxis()).SetLabelSize(0.05);
        SigKinMuProb.SetOption("COLZ1");
        
        SigKinMuProbC=r.TCanvas("wMP", "wMP",910,600)
        SigKinMuProbC.cd();
        SigKinMuProbpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        SigKinMuProbpad.SetTopMargin(0.99) #0.4
        SigKinMuProbpad.SetBottomMargin(0.14) #0.3
        SigKinMuProbpad.SetFillColor(0)
        SigKinMuProbpad.SetFillStyle(0)
        
        SigKinMuProbpad.Draw()
        SigKinMuProbpad.cd()

        SigKinMuProb.Draw("COLZ1")
        
        SigKinMuProbC.Update()
        SigKinMuProbpal = SigKinMuProb.GetListOfFunctions().FindObject("palette")
        SigKinMuProbpal.SetX1NDC(0.91);
        SigKinMuProbpal.SetX2NDC(0.935);
        SigKinMuProbpal.SetY1NDC(0.14);
        SigKinMuProbpal.SetY2NDC(0.9);
        SigKinMuProbpal.SetLabelOffset(-2)
        #wMPpal.SetTitle("weight")
        SigKinMuProbC.Update()
        SigKinMuProbC.SaveAs(home + "/SigKinMuProb"+str(PTlow)+ ".png")

        SigKinPiProb.SetTitle("")
        (SigKinPiProb.GetXaxis()).SetTitleOffset(1.1)
        (SigKinPiProb.GetYaxis()).SetTitleOffset(0.8)
        (SigKinPiProb.GetZaxis()).SetTitleOffset(1.2)
        (SigKinPiProb.GetXaxis()).SetTitle("\phi");
        (SigKinPiProb.GetYaxis()).SetTitle("\eta");
        (SigKinPiProb.GetXaxis()).SetTitleSize(0.05);
        (SigKinPiProb.GetYaxis()).SetTitleSize(0.05);
        (SigKinPiProb.GetXaxis()).SetLabelSize(0.05);
        (SigKinPiProb.GetYaxis()).SetLabelSize(0.05);
        (SigKinPiProb.GetZaxis()).SetTitleSize(0.05);
        (SigKinPiProb.GetZaxis()).SetLabelSize(0.05);
        SigKinPiProb.SetOption("COLZ2");
        
        SigKinPiProbC=r.TCanvas("wMP", "wMP",910,600)
        SigKinPiProbC.cd();
        SigKinPiProbpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        SigKinPiProbpad.SetTopMargin(0.99) #0.4
        SigKinPiProbpad.SetBottomMargin(0.14) #0.3
        SigKinPiProbpad.SetFillColor(0)
        SigKinPiProbpad.SetFillStyle(0)
        
        SigKinPiProbpad.Draw()
        SigKinPiProbpad.cd()

        SigKinPiProb.Draw("COLZ2")

        SigKinPiProbC.Update()
        SigKinPiProbpal = SigKinPiProb.GetListOfFunctions().FindObject("palette")
        SigKinPiProbpal.SetX1NDC(0.91);
        SigKinPiProbpal.SetX2NDC(0.935);
        SigKinPiProbpal.SetY1NDC(0.14);
        SigKinPiProbpal.SetY2NDC(0.9);
        SigKinPiProbpal.SetLabelOffset(-2)

        #wMPpal.SetTitle("weight")
        SigKinPiProbC.Update()
        SigKinPiProbC.SaveAs(home + "/SigKinPiProb"+str(PTlow)+ ".png")
        """
        ###calculate efficiencies by clone associated J/Psi events and dividing by total number of events
        efficiencyMuPlus = JPsiKinPlusAssoc.Clone();
        efficiencyMuMinus = JPsiKinMinusAssoc.Clone();
   
        efficiencyMuPlus.SetName("efficiencyMuPlus");
        efficiencyMuMinus.SetName("efficiencyMuMinus");
   
        efficiencyMuPlus.Divide(JPsiKinPlus);
        efficiencyMuMinus.Divide(JPsiKinMinus);
       
        efficiencyMuPlus.SetTitle("MuPlus Efficiency Control");
        efficiencyMuMinus.SetTitle("MuMinus Efficiency Control");
   
        (efficiencyMuPlus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuPlus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuPlus.GetYaxis()).SetTitle("\eta");
        efficiencyMuPlus.SetOption("COLZ1");
   
        (efficiencyMuMinus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuMinus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuMinus.GetYaxis()).SetTitle("\eta");
        efficiencyMuMinus.SetOption("COLZ1");
   
        efficiencyMuPlus.Write();
        efficiencyMuMinus.Write();
        """
        ###plot efficiencies
        ep=r.TCanvas("ep", "ep",910,600)
        ep.cd();
        efficiencyMuPlus.Draw("COLZ1")

        ep.Update()
        paletteEP = efficiencyMuPlus.GetListOfFunctions().FindObject("palette")
        paletteEP.SetX1NDC(0.91);
        paletteEP.SetX2NDC(0.935);
        paletteEP.SetY1NDC(0.1);
        paletteEP.SetY2NDC(0.9);
        paletteEP.SetLabelOffset(-0.03)
        ep.Update()
       
        ep.Write();
   
        ep.SaveAs(home + "/EfficiencyPlus"+str(PTlow)+ ".png")
               
        em=r.TCanvas("em", "em",910,600)
        em.cd();
        efficiencyMuMinus.Draw("COLZ1")

        em.Update()
        paletteEM = efficiencyMuMinus.GetListOfFunctions().FindObject("palette")
        paletteEM.SetX1NDC(0.91);
        paletteEM.SetX2NDC(0.935);
        paletteEM.SetY1NDC(0.1);
        paletteEM.SetY2NDC(0.9);
        paletteEM.SetLabelOffset(-0.03)
        em.Update()
       
        em.Write();
   
        em.SaveAs(home + "/EfficiencyMinus"+str(PTlow)+ ".png")
        """
        ###calculate nominator and denominator of efficiency equation
        nom = efficiencyMuPlus.Clone();
        den = efficiencyMuPlus.Clone();
   
        nom.Add(efficiencyMuMinus, -1);
        den.Add(efficiencyMuMinus);
   
        ###calculate local asymmetries
        Asymm = nom.Clone();
        Asymm.SetName("Asymm");
   
        Asymm.Divide(den);
        """
        ###plot control tag and probe efficiencies
        #c=r.TCanvas("c", "c",910,600)
        #c.cd();
        
        #(Asymm.GetXaxis()).SetTitleOffset(1.5)
        #(Asymm.GetXaxis()).SetTitle("\Phi");
        #(Asymm.GetYaxis()).SetTitle("\eta");
        #Asymm.GetZaxis().SetTitle("Asymm(%)")
        #Asymm.SetOption("COLZ1");

        Asymm.SetTitle("");
        Asymm.Write();
        Asymm.Draw("COLZ1")

        c.Update()
        paletteA = Asymm.GetListOfFunctions().FindObject("palette")
        paletteA.SetX1NDC(0.91);
        paletteA.SetX2NDC(0.935);
        paletteA.SetY1NDC(0.1);
        paletteA.SetY2NDC(0.9);
        paletteA.SetLabelOffset(-0.03)
        c.Update()
       
        c.Write();
   
        c.SaveAs(home + "/Asymm"+str(PTlow)+ ".png")
        """
        ###weight to signal kinematics
        AsymmMu = Asymm.Clone();
        AsymmMu.SetName("AsymmMu");  

        AsymmPi = Asymm.Clone();
        AsymmPi.SetName("AsymmPi");  

        AsymmMu.Multiply(SigKinMuProb);
   
        AsymmPi.Multiply(SigKinPiProb);
        """
        ###plot signal track asymmetries
        AsymmPi.SetOption("COLZ1");
        AsymmPi.SetTitle("");
        AsymmPi.Write();
    
        AsymmPi.Draw("COLZ1")

        c.Update()
        paletteA = AsymmPi.GetListOfFunctions().FindObject("palette")
        paletteA.SetX1NDC(0.91);
        paletteA.SetX2NDC(0.935);
        paletteA.SetY1NDC(0.1);
        paletteA.SetY2NDC(0.9);
        paletteA.SetLabelOffset(-0.03)
    
        c.Update()
       
        c.Write();
   
        c.SaveAs(home + "/AsymmPiO"+str(PTlow)+ ".png")
        """
        ###calculate total asymmetry
        AsymmTot = AsymmMu.Clone();
        AsymmTot.SetName("AsymmTot" + str(PTlow))
        AsymmTot.SetTitle("");
        AsymmTot.Add(AsymmPi, -1);
   
        #AsymmTot.Multiply(OneHundredHist);
        AsymmTot.Write();
        """
        ###plot distributions of final asymmetries
        d=r.TCanvas("c", "c",910,600)
        d.cd();

        (AsymmTot.GetXaxis()).SetTitleOffset(1.5)
        (AsymmTot.GetXaxis()).SetTitle("\Phi");
        (AsymmTot.GetYaxis()).SetTitle("\eta");
        AsymmTot.Draw("COLZ2")
        d.Update()
        paletteB = AsymmTot.GetListOfFunctions().FindObject("palette")
        paletteB.SetX1NDC(0.91);
        paletteB.SetX2NDC(0.935);
        paletteB.SetY1NDC(0.1);
        paletteB.SetY2NDC(0.9);
        paletteB.SetLabelOffset(-0.03)
        AsymmTot.SetOption("COLZ2")
        d.Update()

        d.Write();
       
        d.SaveAs(home + "/AsymmTot"+str(PTlow)+".png")
        """
        ###obtain values by summing over bins
        TotalAsymm = 0;
        TotalAsymmMu = 0;
        TotalAsymmPi = 0;
        TotalPiProb = 0;
        TotalMuProb = 0;
        AsymmJ = 0;
        efficiencyPlus = 0;
        efficiencyMinus = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                efficiencyPlus = efficiencyPlus + efficiencyMuPlus.GetBinContent(i,j);
                efficiencyMinus = efficiencyMinus + efficiencyMuMinus.GetBinContent(i,j);
                AsymmJ= AsymmJ + Asymm.GetBinContent(i,j)
                TotalAsymm = TotalAsymm + AsymmTot.GetBinContent(i,j)
                TotalAsymmMu = TotalAsymmMu + AsymmMu.GetBinContent(i,j)
                TotalAsymmPi = TotalAsymmPi + AsymmPi.GetBinContent(i,j)
                TotalPiProb = TotalPiProb + SigKinPiProb.GetBinContent(i,j)
                TotalMuProb = TotalMuProb + SigKinMuProb.GetBinContent(i,j)

        ###errors asymmetry JPsi
        ###Plus and Minus
        ###efficiency PLUS region
        ###efficieny error prop
        ###efficieny error prop
        ###muplus
        ###efficiencyMuErrorSquare=eefficiencyMu(1-efficiencyMu)sumsWeightMu
        X=OneHist.Clone()
        X.Add(efficiencyMuPlus, -1)
        efficiencyMuPlusErrorSQUARE=efficiencyMuPlus.Clone()
        efficiencyMuPlusErrorSQUARE.Multiply(X)
        efficiencyMuPlusErrorSQUARE.Divide(JPsiKinPlus)
       
        ###muminus
        Y=OneHist.Clone()
        Y.Add(efficiencyMuMinus, -1)
       
        efficiencyMuMinusErrorSQUARE=efficiencyMuMinus.Clone()
        efficiencyMuMinusErrorSQUARE.Multiply(Y)
        efficiencyMuMinusErrorSQUARE.Divide(JPsiKinMinus)
       
        ###calculating asuymmety errors
        ###error Asymm^2=4((e_+^2*sig_e_-^2 + e_-^2*sig_e_+^2)/(e_+ + e_-)^4)
        ###first bit nom
        beta = efficiencyMuPlus.Clone()
        beta.Multiply(efficiencyMuPlus)
        beta.Multiply(efficiencyMuMinusErrorSQUARE)
        ###second bit nom
        gamma = efficiencyMuMinus.Clone()
        gamma.Multiply(efficiencyMuMinus)
        gamma.Multiply(efficiencyMuPlusErrorSQUARE)
        
        beta.Add(gamma)
        ###den
        denFOURTH = den.Clone()
        denFOURTH.Multiply(den)
        denFOURTH.Multiply(den)
        denFOURTH.Multiply(den)
        ###divide
        beta.Divide(denFOURTH)
        ###*4
        AsymmErrorSQUARE = beta.Clone()
        AsymmErrorSQUARE.Add(beta)
        AsymmErrorSQUARE.Add(beta)
        AsymmErrorSQUARE.Add(beta)
   
        ###calculate errors in each bin
        JMuPlusError = 0;
        TotalJPsiError = 0;
        TotalJPsiError = 0;

        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                JMuPlusError = JMuPlusError + efficiencyMuPlusErrorSQUARE.GetBinContent(i,j);
                JMuMinusError = JMuMinusError + efficiencyMuMinusErrorSQUARE.GetBinContent(i,j);
                TotalJPsiError = TotalJPsiError + AsymmErrorSQUARE.GetBinContent(i,j);

        ###calculate integrated efficiencies and asymmetries with error in each PT slice
        JPsiKinPlusTot = 0;
        JPsiKinMinusTot = 0;
        JPsiKinPlusAssocTot = 0;
        JPsiKinMinusAssocTot = 0;

        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                JPsiKinPlusTot = JPsiKinPlusTot + JPsiKinPlus.GetBinContent(i,j);
                JPsiKinMinusTot = JPsiKinMinusTot + JPsiKinMinus.GetBinContent(i,j);
                JPsiKinPlusAssocTot = JPsiKinPlusAssocTot + JPsiKinPlusAssoc.GetBinContent(i,j);
                JPsiKinMinusAssocTot = JPsiKinMinusAssocTot + JPsiKinMinusAssoc.GetBinContent(i,j);
        
        efficiencyPlus = JPsiKinPlusAssocTot/JPsiKinPlusTot
        efficiencyMinus = JPsiKinMinusAssocTot/JPsiKinMinusTot
        
        
        AsymmJ = (efficiencyPlus-efficiencyMinus)/(efficiencyPlus+efficiencyMinus);
        
        errorEPlusSQUARE = efficiencyPlus*(1-efficiencyPlus)/JPsiKinPlusTot;
        errorEMinusSQUARE = efficiencyMinus*(1-efficiencyMinus)/JPsiKinMinusTot;

        errorNom = 4*(efficiencyPlus*efficiencyPlus*errorEMinusSQUARE + efficiencyMinus*efficiencyMinus*errorEPlusSQUARE);
        Beta = efficiencyPlus + efficiencyMinus;
        errorDen = Beta * Beta * Beta * Beta;
        errorAsymm = errorNom/errorDen
       
        ###ensure error^2 is positive
        if (GlobalErrorAsymmSQUARE < 0):
            modGEA =  -GlobalErrorAsymmSQUARE;
            GlobalErrorAsymmSQUARE = modGEA;
       
        ###calculate correlation coefficent
        DMuQuadSum = 0;
        DPiQuadSum = 0;
        DMuPiSum = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                DMuQuadSum = DMuQuadSum + DMuQuadName.GetBinContent(i,j);
                DPiQuadSum = DPiQuadSum + DPiQuadName.GetBinContent(i,j);
                DMuPiSum = DMuPiSum + DMuName.GetBinContent(i,j)*DPiName.GetBinContent(i,j);
        rho = [];
        rho.append(-1)
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(-1)
        
        ###obtain deltaAMu
        deltaAMuSquare = 0;
        deltaAPiSquare = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1): 
                deltaAMuSquare =  deltaAMuSquare + (DMuQuadName.GetBinContent(i,j)*AsymmErrorSQUARE.GetBinContent(i,j))
                deltaAPiSquare =  deltaAPiSquare + (DPiQuadName.GetBinContent(i,j)*AsymmErrorSQUARE.GetBinContent(i,j))
                #print(DPiQuadName.GetBinContent(i,j))
        if (deltaAMuSquare < 0):
            deltaAMuSquare = -deltaAMuSquare;
        if (deltaAPiSquare < 0):
            deltaAPiSquare = -deltaAPiSquare;            
        deltaAMu = np.sqrt(deltaAMuSquare)/NSIGMu
        deltaAPi = np.sqrt(deltaAPiSquare)/NSIGPi    
        ###covariance matrix calculated
        V = []
        V.append(rho[0]* deltaAMu *  deltaAMu)
        V.append(rho[1]* deltaAMu *  deltaAPi)
        V.append(rho[2]* deltaAPi *  deltaAMu)
        V.append(rho[3]* deltaAPi *  deltaAPi)
        
        Vdet = V[0]*V[3]-V[1]*V[2];
        
        Vinverse = [];
        Vinverse.append(V[3]/Vdet)
        Vinverse.append(-V[1]/Vdet)
        Vinverse.append(-V[2]/Vdet)
        Vinverse.append(V[0]/Vdet)
        
        VinverseSum = 0;
        VinverseMuSum = Vinverse[0]+Vinverse[1];
        VinversePiSum = Vinverse[2]+Vinverse[3];     
        
        for i in range(0,3):
            VinverseSum = VinverseSum + Vinverse[i];
        ###omega used to calculate correlated error
        Omega=[]
                    
        Omega.append(VinverseMuSum/VinverseSum)
        Omega.append(VinversePiSum/VinverseSum)
        
        DeltaATrack = Omega[0]*Omega[0]*V[0]
        DeltaATrack = DeltaATrack+Omega[0]*Omega[1]*V[1]
        DeltaATrack = DeltaATrack+Omega[1]*Omega[0]*V[2]
        DeltaATrack = DeltaATrack+Omega[1]*Omega[1]*V[3]
        if (DeltaATrack < 0):
           DeltaATrack = -DeltaATrack; 
        ###correlated error
        DeltaATrack = np.sqrt(DeltaATrack)

        ###stor variables in array to be saved in a csv file
        if (systematic == True):
            PTlowLS.append(PTlow)
            efficiencyPlusLS.append(efficiencyPlus)
            efficiencyMinusLS.append(efficiencyMinus)
            errorEPlusLS.append(np.sqrt(errorEPlusSQUARE))
            errorEMinusLS.append(np.sqrt(errorEMinusSQUARE))
            AsymmJLS.append(AsymmJ*100)
            errorAsymmLS.append(np.sqrt(errorAsymm)*100)
            GlobalAsymmLS.append(TotalAsymm*100)
            GlobalAsymmELS.append(np.sqrt(GlobalErrorAsymmSQUARE)*100)
            MuAsymmLS.append(TotalAsymmMu*100)
            MuAsymmELS.append(np.sqrt(TotalMuError)*100)
            PiAsymmELS.append(np.sqrt(TotalPiError)*100)
            PiAsymmLS.append(TotalAsymmPi*100)
            CorrErrS.append(DeltaATrack*100)
      
        else:
            PTlowL.append(PTlow)
            efficiencyPlusL.append(efficiencyPlus)
            efficiencyMinusL.append(efficiencyMinus)
            errorEPlusL.append(np.sqrt(errorEPlusSQUARE))
            errorEMinusL.append(np.sqrt(errorEMinusSQUARE))
            AsymmJL.append(AsymmJ*100)
            errorAsymmL.append(np.sqrt(errorAsymm)*100)
            GlobalAsymmL.append(TotalAsymm*100)
            GlobalAsymmEL.append(np.sqrt(GlobalErrorAsymmSQUARE)*100)
            MuAsymmL.append(TotalAsymmMu*100)
            MuAsymmEL.append(np.sqrt(TotalMuError)*100)
            PiAsymmEL.append(np.sqrt(TotalPiError)*100)
            PiAsymmL.append(TotalAsymmPi*100)
            CorrErr.append(DeltaATrack*100)
      
    ###iterate over for orignal PT slices
    for i in xrange(sliceNumber):
        TwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTSlicesD["PTlow"+str(i)], home, sliceNumber,  False)
    ###iterate over for systematic PT slices
    for i in xrange(sliceNumberSyst):
        TwoDHistSlices(DHistsSyst, JPlusHistsSyst, JMinusHistsSyst, polarity, PTSlicesDSyst["PTlow"+str(i)], home, sliceNumberSyst, True)
    print("Phase Binning method completed")
    ###output results to a csv
    data = {"$PT low (MeV)$":PTlowL,
            "$Efficiency Plus$" :efficiencyPlusL,
            "$Error Efficiencty Plus$": errorEPlusL,
            "$Efficiency Minus$" :efficiencyMinusL,
            "$Error Efficieny Minus$":errorEMinusL,
            "$Asymmetry (percent)$" : AsymmJL,
            "$error Asymmetry (percent)$": errorAsymmL,
            "$Mu Asymmetry (percent)$": MuAsymmL,
            "$Mu Asymm error (percent)$": MuAsymmEL,
            "$Pi Asymmetry (percent)$": PiAsymmL,
            "$Pi Asymm error (percent)$": PiAsymmEL,
            "$Global Asymmetry (percent)$": GlobalAsymmL,
            "$Global Asymmetry Error (percent)$": GlobalAsymmEL,
            "$Global Asymmetry Error Corr (percent)$": CorrErr
            }
    df=pd.DataFrame(data)
    column_order = ["$PT low (MeV)$",
            "$Efficiency Plus$",
            "$Error Efficiencty Plus$",
            "$Efficiency Minus$",
            "$Error Efficieny Minus$",
            "$Asymmetry (percent)$",
            "$error Asymmetry (percent)$",
            "$Mu Asymmetry (percent)$",
            "$Mu Asymm error (percent)$",
            "$Pi Asymmetry (percent)$",
            "$Pi Asymm error (percent)$",
            "$Global Asymmetry (percent)$",
            "$Global Asymmetry Error (percent)$",
            "$Global Asymmetry Error Corr (percent)$"]
    df[column_order].to_csv('2DSlicesdata'+ polarity + 'PTSlicedFits.csv', index = None)

    dataS = {"$PT low (MeV)$":PTlowLS,
            "$Efficiency Plus$" :efficiencyPlusLS,
            "$Error Efficiencty Plus$": errorEPlusLS,
            "$Efficiency Minus$" :efficiencyMinusLS,
            "$Error Efficieny Minus$":errorEMinusLS,
            "$Asymmetry (percent)$" : AsymmJLS,
            "$error Asymmetry (percent)$": errorAsymmLS,
            "$Mu Asymmetry (percent)$": MuAsymmLS,
            "$Mu Asymm error (percent)$": MuAsymmELS,
            "$Pi Asymmetry (percent)$": PiAsymmLS,
            "$Pi Asymm error (percent)$": PiAsymmELS,
            "$Global Asymmetry (percent)$": GlobalAsymmLS,
            "$Global Asymmetry Error (percent)$": GlobalAsymmELS,
            "$Global Asymmetry Error Corr (percent)$": CorrErrS
            }
    dfS=pd.DataFrame(dataS)
    column_order = ["$PT low (MeV)$",
            "$Efficiency Plus$",
            "$Error Efficiencty Plus$",
            "$Efficiency Minus$",
            "$Error Efficieny Minus$",
            "$Asymmetry (percent)$",
            "$error Asymmetry (percent)$",
            "$Mu Asymmetry (percent)$",
            "$Mu Asymm error (percent)$",
            "$Pi Asymmetry (percent)$",
            "$Pi Asymm error (percent)$",
            "$Global Asymmetry (percent)$",
            "$Global Asymmetry Error (percent)$",
            "$Global Asymmetry Error Corr (percent)$"]
    dfS[column_order].to_csv('2DSlicesdata'+ polarity + 'PTSlicedFitsSyst.csv', index = None)
    
    ###arrays to save results from separate reweighting method
    PTlowLSR = []
    efficiencyMuPlusLSR = []
    efficiencyMuMinusLSR = []
    errorEMuPlusLSR = []
    errorEMuMinusLSR = []
    efficiencyPiPlusLSR = []
    efficiencyPiMinusLSR = []
    errorEPiPlusLSR = []
    errorEPiMinusLSR = []
    GlobalAsymmLSR = []
    GlobalAsymmELSR = []
    MuAsymmLSR = []
    MuAsymmELSR = []
    PiAsymmELSR = []
    PiAsymmLSR = []
    GlobalAsymmCorrErrSR = []
    
    PTlowLSRS = []
    efficiencyMuPlusLSRS = []
    efficiencyMuMinusLSRS = []
    errorEMuPlusLSRS = []
    errorEMuMinusLSRS = []
    efficiencyPiPlusLSRS= []
    efficiencyPiMinusLSRS = []
    errorEPiPlusLSRS = []
    errorEPiMinusLSRS = []
    GlobalAsymmLSRS = []
    GlobalAsymmELSRS = []
    MuAsymmLSRS = []
    MuAsymmELSRS = []
    PiAsymmELSRS = []
    PiAsymmLSRS = []
    GlobalAsymmCorrErrSRS = []

 
    def SepReweightTwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTlow, home, sliceNumber, systematic):
        file1 = TFile.Open(DHists)
        file2 = TFile.Open(JPlusHists)
        file3 = TFile.Open(JMinusHists)
        DMu3D = TH3F();
        file1.GetObject("SignalKinMu"+polarity, DMu3D);       
        
        DPi3D = TH3F();
        file1.GetObject("SignalKinPion"+polarity, DPi3D);
        
        DMuName = TH2F();
        file1.GetObject("DMu2D"+polarity+str(PTlow), DMuName);
        
        DMuQuadName = TH2F();
        file1.GetObject("DMuQUAD2D"+polarity+str(PTlow), DMuQuadName);        
       
        DPiQuadName = TH2F();
        file1.GetObject("DPiQUAD2D"+polarity+str(PTlow), DPiQuadName);   
        
        DPiName = TH2F();
        file1.GetObject("DPi2D"+polarity+str(PTlow), DPiName);
       
        JMinusName = TH2F();
        file2.GetObject("JPsiKin"+ polarity + "Plus"+ str(PTlow),JMinusName);
       
        JPlusName = TH2F();
        file3.GetObject("JPsiKin"+ polarity + "Minus" + str(PTlow), JPlusName);
       
        JMinusAssocName = TH2F();
        file2.GetObject("JPsiKinAssoc"+ polarity + "Plus"+ str(PTlow),JMinusAssocName);
       
        JPlusAssocName = TH2F();
        file3.GetObject("JPsiKinAssoc"+ polarity + "Minus" + str(PTlow),JPlusAssocName);

        ###total signal muon and pion events calculated
        fout.cd();
        nSigMu = 0;
        nSigPi = 0;
        fout.cd();
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):           
                for k in xrange(sliceNumber+1):            
                    nSigMu = nSigMu + DMu3D.GetBinContent(i,j,k);
                    nSigPi = nSigPi + DPi3D.GetBinContent(i,j,k);
            
        ###full J/Psi kin plus and minus events
        JPsiKinMinus = JMinusName.Clone();
        JPsiKinPlus = JPlusName.Clone();

        ###matched J/Psi kin plus and minus events
        JPsiKinPlusAssoc = JPlusAssocName.Clone();
        JPsiKinMinusAssoc =  JMinusAssocName.Clone();
       
        ###number of signal Mu events in each kinematic bin
        SigKinMu = DMuName.Clone();
        SigKinMu.Multiply(DMuName);
        SigKinMu.Divide(DMuQuadName)

        ###number of signal Pi events in each kinematic bin
        SigKinPi = DPiName.Clone();
        SigKinPi.Multiply(DPiName);
        SigKinPi.Divide(DPiQuadName);

        TotSigMu = 0;
        TotSigPi = 0;

        ###integrated effeciency and total signal events calculated in each PT slice
        JPsiKinPlusTot = 0;
        JPsiKinMinusTot = 0;
        JPsiKinPlusAssocTot = 0;
        JPsiKinMinusAssocTot = 0;
       
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                    TotSigMu = TotSigMu + SigKinMu.GetBinContent(i,j);
                    TotSigPi = TotSigPi + SigKinPi.GetBinContent(i,j);
                    JPsiKinPlusTot = JPsiKinPlusTot + JPsiKinPlus.GetBinContent(i,j);
                    JPsiKinMinusTot = JPsiKinMinusTot + JPsiKinMinus.GetBinContent(i,j);
                    JPsiKinPlusAssocTot = JPsiKinPlusAssocTot + JPsiKinPlusAssoc.GetBinContent(i,j);
                    JPsiKinMinusAssocTot = JPsiKinMinusAssocTot + JPsiKinMinusAssoc.GetBinContent(i,j);    
        efficiencyPlus = JPsiKinPlusAssocTot/JPsiKinPlusTot;
        efficiencyMinus = JPsiKinMinusAssocTot/JPsiKinMinusTot;
        
        ###histograms storing total number of signal muon and pion events and 
        ###1 and 100 created and filled
        TotSigKinMu = SigKinMu.Clone();
        TotSigKinPi = SigKinPi.Clone();
        OneHist = SigKinPi.Clone();
        OneHist.SetName("one");
        OneHundredHist = SigKinPi.Clone();
        OneHundredHist.SetName("oneHundred");
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                    TotSigKinMu.SetBinContent(i,j, TotSigMu);
                    TotSigKinPi.SetBinContent(i,j, TotSigPi);
                    OneHist.SetBinContent(i,j, 1);
                    OneHundredHist.SetBinContent(i,j, 100);

        ###prob density of muons and pions calculated
        SigKinMuProb = SigKinMu.Clone();
        SigKinMuProb.Divide(TotSigKinMu);
        SigKinMuProb.SetName("SigKinMuProb");
       
        SigKinPiProb = SigKinPi.Clone();
        SigKinPiProb.Divide(TotSigKinPi);
        SigKinPiProb.SetName("SigKinPiProb");
       
        SigKinDifProb = SigKinMuProb.Clone();
        SigKinDifProb.Add(SigKinPiProb, -1);
        SigKinDifProb.SetName("SigKinDifProb");
       
        ###calculate efficiencies by clone associated J/Psi events and dividing by total number of events
        efficiencyMuPlus = JPsiKinPlusAssoc.Clone();
        efficiencyMuMinus = JPsiKinMinusAssoc.Clone();
   
        efficiencyMuPlus.SetName("efficiencyMuPlus");
        efficiencyMuMinus.SetName("efficiencyMuMinus");
   
        efficiencyMuPlus.Divide(JPsiKinPlus);
        efficiencyMuMinus.Divide(JPsiKinMinus);
       
        efficiencyMuPlus.SetTitle("MuPlus Efficiency Control");
        efficiencyMuMinus.SetTitle("MuMinus Efficiency Control");
   
        (efficiencyMuPlus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuPlus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuPlus.GetYaxis()).SetTitle("\eta");
        efficiencyMuPlus.SetOption("BOX2Z");
   
        (efficiencyMuMinus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuMinus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuMinus.GetYaxis()).SetTitle("\eta");
        efficiencyMuMinus.SetOption("BOX2Z");
   
        efficiencyMuPlus.Write();
        efficiencyMuMinus.Write();
       
        ###mu weighting for efficiency plus
        wMuPlus = SigKinMu.Clone();
        wMuPlus.Divide(JPsiKinPlusAssoc);
        """
        wMuPlus.SetTitle("")
        (wMuPlus.GetXaxis()).SetTitleOffset(1.1)
        (wMuPlus.GetYaxis()).SetTitleOffset(0.8)
        (wMuPlus.GetZaxis()).SetTitleOffset(0.8)
        (wMuPlus.GetXaxis()).SetTitle("\phi");
        (wMuPlus.GetYaxis()).SetTitle("\eta");
        (wMuPlus.GetZaxis()).SetTitle("weight");
        (wMuPlus.GetXaxis()).SetTitleSize(0.05);
        (wMuPlus.GetYaxis()).SetTitleSize(0.05);
        (wMuPlus.GetXaxis()).SetLabelSize(0.05);
        (wMuPlus.GetYaxis()).SetLabelSize(0.05);
        (wMuPlus.GetZaxis()).SetTitleSize(0.05);
        (wMuPlus.GetZaxis()).SetLabelSize(0.05);
        wMuPlus.SetOption("COLZ1");
        
        wMP=r.TCanvas("wMP", "wMP",910,600)
        wMP.cd();
        wMPpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        wMPpad.SetTopMargin(0.99) #0.4
        wMPpad.SetBottomMargin(0.14) #0.3
        wMPpad.SetFillColor(0)
        wMPpad.SetFillStyle(0)
        
        wMPpad.Draw()
        wMPpad.cd()

        wMuPlus.Draw("COLZ1")
        
        wMP.Update()
        wMPpal = wMuPlus.GetListOfFunctions().FindObject("palette")
        wMPpal.SetX1NDC(0.91);
        wMPpal.SetX2NDC(0.935);
        wMPpal.SetY1NDC(0.14);
        wMPpal.SetY2NDC(0.9);
        wMPpal.SetLabelOffset(-2)
        #wMPpal.SetTitle("weight")
        wMP.Update()
        wMP.SaveAs(home + "/WMPUp"+str(PTlow)+ ".png")
        """
        ###mu weighting for efficiency minus
        wMuMinus = SigKinMu.Clone();
        wMuMinus.Divide(JPsiKinMinusAssoc);
        """
        wMuMinus.SetTitle("")
        (wMuMinus.GetXaxis()).SetTitleOffset(1.1)
        (wMuMinus.GetYaxis()).SetTitleOffset(0.8)
        (wMuMinus.GetZaxis()).SetTitleOffset(0.8)
        (wMuMinus.GetXaxis()).SetTitle("\phi");
        (wMuMinus.GetYaxis()).SetTitle("\eta");
        (wMuMinus.GetZaxis()).SetTitle("weight");
        (wMuMinus.GetXaxis()).SetTitleSize(0.05);
        (wMuMinus.GetYaxis()).SetTitleSize(0.05);
        (wMuMinus.GetXaxis()).SetLabelSize(0.05);
        (wMuMinus.GetYaxis()).SetLabelSize(0.05);
        (wMuMinus.GetZaxis()).SetTitleSize(0.05);
        (wMuMinus.GetZaxis()).SetLabelSize(0.05);
        wMuMinus.SetOption("COLZ1");
        
        wMM=r.TCanvas("wMP", "wMP",910,600)
        wMM.cd();
        wMMpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        wMMpad.SetTopMargin(0.99) #0.4
        wMMpad.SetBottomMargin(0.14) #0.3
        wMMpad.SetFillColor(0)
        wMMpad.SetFillStyle(0)
        
        wMMpad.Draw()
        wMMpad.cd()
        
        wMuMinus.Draw("COLZ1")
        
        wMM.Update()
        wMMpal = wMuMinus.GetListOfFunctions().FindObject("palette")
        wMMpal.SetX1NDC(0.91);
        wMMpal.SetX2NDC(0.935);
        wMMpal.SetY1NDC(0.14);
        wMMpal.SetY2NDC(0.9);
        wMMpal.SetLabelOffset(-2)
        #wMPpal.SetTitle("weight")
        wMM.Update()
        wMM.SaveAs(home + "/WMMUp"+str(PTlow)+ ".png")
        """
        
        ###pi weighting for efficiency plus
        wPiPlus = SigKinPi.Clone();
        wPiPlus.Divide(JPsiKinPlusAssoc);
        
        """
        wPiPlus.SetTitle("")
        (wPiPlus.GetXaxis()).SetTitleOffset(1.1)
        (wPiPlus.GetYaxis()).SetTitleOffset(0.8)
        (wPiPlus.GetZaxis()).SetTitleOffset(0.8)
        (wPiPlus.GetXaxis()).SetTitle("\phi");
        (wPiPlus.GetYaxis()).SetTitle("\eta");
        (wPiPlus.GetZaxis()).SetTitle("weight");
        (wPiPlus.GetXaxis()).SetTitleSize(0.05);
        (wPiPlus.GetYaxis()).SetTitleSize(0.05);
        (wPiPlus.GetXaxis()).SetLabelSize(0.05);
        (wPiPlus.GetYaxis()).SetLabelSize(0.05);
        (wPiPlus.GetZaxis()).SetTitleSize(0.05);
        (wPiPlus.GetZaxis()).SetLabelSize(0.05);
        wPiPlus.SetOption("COLZ1");
        
        wPP=r.TCanvas("wPM", "wPM",910,600)
        wPP.cd();
        wPPpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        wPPpad.SetTopMargin(0.99) #0.4
        wPPpad.SetBottomMargin(0.14) #0.3
        wPPpad.SetFillColor(0)
        wPPpad.SetFillStyle(0)
        
        wPPpad.Draw()
        wPPpad.cd()

        wPiPlus.Draw("COLZ1")
        
        wPP.Update()
        wPPpal = wPiPlus.GetListOfFunctions().FindObject("palette")
        wPPpal.SetX1NDC(0.91);
        wPPpal.SetX2NDC(0.935);
        wPPpal.SetY1NDC(0.14);
        wPPpal.SetY2NDC(0.9);
        wPPpal.SetLabelOffset(-2)
        #wMPpal.SetTitle("weight")
        wPP.Update()
        wPP.SaveAs(home + "/WPPUp"+str(PTlow)+ ".png")
        """
       
        ###pi weighting for efficiency minus
        wPiMinus = SigKinPi.Clone();
        wPiMinus.Divide(JPsiKinMinusAssoc);
        """
        wPiMinus.SetTitle("")
        (wPiMinus.GetXaxis()).SetTitleOffset(1.1)
        (wPiMinus.GetYaxis()).SetTitleOffset(0.8)
        (wPiMinus.GetZaxis()).SetTitleOffset(0.8)
        (wPiMinus.GetXaxis()).SetTitle("\phi");
        (wPiMinus.GetYaxis()).SetTitle("\eta");
        (wPiMinus.GetZaxis()).SetTitle("weight");
        (wPiMinus.GetXaxis()).SetTitleSize(0.05);
        (wPiMinus.GetYaxis()).SetTitleSize(0.05);
        (wPiMinus.GetXaxis()).SetLabelSize(0.05);
        (wPiMinus.GetYaxis()).SetLabelSize(0.05);
        (wPiMinus.GetZaxis()).SetTitleSize(0.05);
        (wPiMinus.GetZaxis()).SetLabelSize(0.05);
        wPiMinus.SetOption("COLZ1");
        
        wPM=r.TCanvas("wPM", "wPM",910,600)
        wPM.cd();
        wPMpad = TPad("pad", "pad", 0.02, 0.02, 0.96, 0.96)
        wPMpad.SetTopMargin(0.99) #0.4
        wPMpad.SetBottomMargin(0.14) #0.3
        wPMpad.SetFillColor(0)
        wPMpad.SetFillStyle(0)
        
        wPMpad.Draw()
        wPMpad.cd()

        wPiMinus.Draw("COLZ1")
        
        wPM.Update()
        wPMpal = wPiMinus.GetListOfFunctions().FindObject("palette")
        wPMpal.SetX1NDC(0.91);
        wPMpal.SetX2NDC(0.935);
        wPMpal.SetY1NDC(0.14);
        wPMpal.SetY2NDC(0.9);
        wPMpal.SetLabelOffset(-2)
        #wMPpal.SetTitle("weight")
        wPM.Update()
        wPM.SaveAs(home + "/WPMUp"+str(PTlow)+ ".png")
        
        """
        
        ###mu weighted efficiency plus
        wMuPlusEff = wMuPlus.Clone();
        wMuPlusEff.Multiply(efficiencyMuPlus);

        ###mu weighted efficiency minus     
        wMuMinusEff = wMuMinus.Clone();
        wMuMinusEff.Multiply(efficiencyMuMinus);
       
        ###pi weighted efficiency plus
        wPiPlusEff = wPiPlus.Clone();
        wPiPlusEff.Multiply(efficiencyMuPlus);
       
        ###pi weighted efficiency minus
        wPiMinusEff = wPiMinus.Clone();
        wPiMinusEff.Multiply(efficiencyMuMinus);
        
        ###mu track asymm calculated
        AsymmMu = wMuPlusEff.Clone();
        AsymmMu.Add(wMuMinusEff, -1);
       
        MuDen = wMuPlusEff.Clone();
        MuDen.Add(wMuMinusEff);
       
        AsymmMu.Divide(MuDen);
        AsymmMu.Multiply(SigKinMuProb);
        ###pi track asymm calculated
        AsymmPi = wPiPlusEff.Clone();
        AsymmPi.Add(wPiMinusEff, -1);
       
        PiDen = wPiPlusEff.Clone();
        PiDen.Add(wPiMinusEff);
       
        AsymmPi.Divide(PiDen);
        AsymmPi.Multiply(SigKinPiProb);

        ###mu pi track asymm calculated
        AsymmTot = AsymmMu.Clone();
        AsymmTot.Add(AsymmPi, -1);
        
        ###error on raw efficiencies calculated
        X=OneHist.Clone()
        X.Add(efficiencyMuPlus, -1)
        efficiencyMuPlusErrorSQUARE=efficiencyMuPlus.Clone()
        efficiencyMuPlusErrorSQUARE.Multiply(X)
        efficiencyMuPlusErrorSQUARE.Divide(JPsiKinPlus)

        ###muminus
        Y=OneHist.Clone()
        Y.Add(efficiencyMuMinus, -1)
       
        efficiencyMuMinusErrorSQUARE=efficiencyMuMinus.Clone()
        efficiencyMuMinusErrorSQUARE.Multiply(Y)
        efficiencyMuMinusErrorSQUARE.Divide(JPsiKinMinus)
        ###error on sigMu Control Mu+ (probe) weight calculated     

        ###error on nominal JPsiAssoc Term Calculated Binomially
        JPlusAssocError=efficiencyMuPlus.Clone()
        JPlusAssocError.Multiply(X)
        JPlusAssocError.Multiply(JPsiKinPlus)
        
        JMinusAssocError=efficiencyMuMinus.Clone()
        JMinusAssocError.Multiply(Y)
        JMinusAssocError.Multiply(JPsiKinMinus)
        
        ###error on sigMu Control Mu+ weight calculated
        errorWMuPlusSQUAREDTerm1 = SigKinMu.Clone(); 
        errorWMuPlusSQUAREDTerm1.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUAREDTerm1.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED = errorWMuPlusSQUAREDTerm1.Clone()
        
        errorWMuPlusSQUARED = SigKinMu.Clone();
        errorWMuPlusSQUARED.Multiply(SigKinMu);
        errorWMuPlusSQUARED.Multiply(JPlusAssocError);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        
        errorWMuPlusSQUARED.Add(errorWMuPlusSQUAREDTerm1);

        ###error on sigPi Control Mu+ weight calculated
        errorWPiPlusSQUAREDTerm1 = SigKinPi.Clone(); 
        errorWPiPlusSQUAREDTerm1.Divide(JPsiKinPlusAssoc);
        errorWPiPlusSQUAREDTerm1.Divide(JPsiKinPlusAssoc);
        errorWPiPlusSQUARED = errorWPiPlusSQUAREDTerm1.Clone()
        
        errorWPiPlusSQUARED = SigKinPi.Clone();
        errorWPiPlusSQUARED.Multiply(SigKinPi);
        errorWMuPlusSQUARED.Multiply(JPlusAssocError);
        errorWPiPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWPiPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        errorWMuPlusSQUARED.Divide(JPsiKinPlusAssoc);
        
        errorWPiPlusSQUARED.Add(errorWPiPlusSQUAREDTerm1);
    
        ###error on sigMu Control Mu- weight calculated
        errorWMuMinusSQUAREDTerm1 = SigKinMu.Clone(); 
        errorWMuMinusSQUAREDTerm1.Divide(JPsiKinMinusAssoc);
        errorWMuMinusSQUAREDTerm1.Divide(JPsiKinMinusAssoc);
        errorWMuMinusSQUARED = errorWMuPlusSQUAREDTerm1.Clone()
        
        errorWMuMinusSQUARED = SigKinMu.Clone();
        errorWMuMinusSQUARED.Multiply(SigKinMu);
        errorWMuMinusSQUARED.Divide(JPsiKinMinusAssoc);
        errorWMuMinusSQUARED.Divide(JPsiKinMinusAssoc);
        errorWMuMinusSQUARED.Divide(JPsiKinMinusAssoc);
        
        errorWMuMinusSQUARED.Add(errorWMuMinusSQUAREDTerm1);
        
        ###error on sigPi Control Mu- weight calculated
        errorWPiMinusSQUAREDTerm1 = SigKinPi.Clone(); 
        errorWPiMinusSQUAREDTerm1.Divide(JPsiKinMinusAssoc);
        errorWPiMinusSQUAREDTerm1.Divide(JPsiKinMinusAssoc);
        errorWPiMinusSQUARED = errorWPiMinusSQUAREDTerm1.Clone()

        errorWPiMinusSQUARED = SigKinPi.Clone();
        errorWPiMinusSQUARED.Multiply(SigKinPi);
        errorWPiMinusSQUARED.Divide(JPsiKinMinusAssoc);
        errorWPiMinusSQUARED.Divide(JPsiKinMinusAssoc);
        errorWPiMinusSQUARED.Divide(JPsiKinMinusAssoc);

        errorWPiMinusSQUARED.Add(errorWPiMinusSQUAREDTerm1);
        
        
        ###errors of 4 weighted efficiencies calculated using
        ###error_(wa)^2 = error_w^2*a^2 + error_a^2 * w^2
        errorWMuPlusEffSQUARED = efficiencyMuPlus.Clone()
        errorWMuPlusEffSQUARED.Multiply(efficiencyMuPlus)
        errorWMuPlusEffSQUARED.Multiply(errorWMuPlusSQUARED)
        errorWMuPlusEffSQUAREDAlpha = wMuPlus.Clone()
        errorWMuPlusEffSQUAREDAlpha.Multiply(wMuPlus)
        errorWMuPlusEffSQUAREDAlpha.Multiply(efficiencyMuPlusErrorSQUARE) 
        errorWMuPlusEffSQUARED.Add(errorWMuPlusEffSQUAREDAlpha)
        
        errorWMuMinusEffSQUARED = efficiencyMuMinus.Clone()
        errorWMuMinusEffSQUARED.Multiply(efficiencyMuMinus)
        errorWMuMinusEffSQUARED.Multiply(errorWMuMinusSQUARED)
        errorWMuMinusEffSQUAREDAlpha = wMuMinus.Clone()
        errorWMuMinusEffSQUAREDAlpha.Multiply(wMuMinus)
        errorWMuMinusEffSQUAREDAlpha.Multiply(efficiencyMuMinusErrorSQUARE) 
        errorWMuMinusEffSQUARED.Add(errorWMuMinusEffSQUAREDAlpha)
        
        errorWPiPlusEffSQUARED = efficiencyMuPlus.Clone()
        errorWPiPlusEffSQUARED.Multiply(efficiencyMuPlus)
        errorWPiPlusEffSQUARED.Multiply(errorWPiPlusSQUARED)
        errorWPiPlusEffSQUAREDAlpha = wPiPlus.Clone()
        errorWPiPlusEffSQUAREDAlpha.Multiply(wPiPlus)
        errorWPiPlusEffSQUAREDAlpha.Multiply(efficiencyMuPlusErrorSQUARE) 
        errorWPiPlusEffSQUARED.Add(errorWPiPlusEffSQUAREDAlpha)
        
        errorWPiMinusEffSQUARED = efficiencyMuMinus.Clone()
        errorWPiMinusEffSQUARED.Multiply(efficiencyMuMinus)
        errorWPiMinusEffSQUARED.Multiply(errorWPiMinusSQUARED)
        errorWPiMinusEffSQUAREDAlpha = wPiMinus.Clone()
        errorWPiMinusEffSQUAREDAlpha.Multiply(wPiMinus)
        errorWPiMinusEffSQUAREDAlpha.Multiply(efficiencyMuMinusErrorSQUARE) 
        errorWPiMinusEffSQUARED.Add(errorWPiMinusEffSQUAREDAlpha)
        
        ###calculating asuymmety errors for muon track
        ###error Asymm^2=4((e_+^2*sig_e_-^2 + e_-^2*sig_e_+^2)/(e_+ + e_-)^4)
        ###first bit nom
        betaMu = wMuPlusEff.Clone()
        betaMu.Multiply(wMuPlusEff)
        betaMu.Multiply(errorWMuMinusEffSQUARED)
        ###second bit nom
        gammaMu = efficiencyMuMinus.Clone()
        gammaMu.Multiply(efficiencyMuMinus)
        gammaMu.Multiply(errorWMuPlusEffSQUARED)
        ###add
        betaMu.Add(gammaMu)
        ###den
        denFOURTHMu = MuDen.Clone()
        denFOURTHMu.Multiply(MuDen)
        denFOURTHMu.Multiply(MuDen)
        denFOURTHMu.Multiply(MuDen)
        ###divide
        betaMu.Divide(denFOURTHMu)
        ###*4
        AsymmMuErrorSQUARE = betaMu.Clone()
        AsymmMuErrorSQUARE.Add(betaMu)
        AsymmMuErrorSQUARE.Add(betaMu)
        AsymmMuErrorSQUARE.Add(betaMu)
        
        ###calculating asuymmety errors for muon track
        ###error Asymm^2=4((e_+^2*sig_e_-^2 + e_-^2*sig_e_+^2)/(e_+ + e_-)^4)
        ###first bit nom
        betaPi = wPiPlusEff.Clone()
        betaPi.Multiply(wPiPlusEff)
        betaPi.Multiply(errorWPiMinusEffSQUARED)
        ###second bit nom
        gammaPi = wPiMinusEff.Clone()
        gammaPi.Multiply(wPiMinusEff)
        gammaPi.Multiply(errorWPiPlusEffSQUARED)
        ###add
        betaPi.Add(gammaPi)
        #den
        denFOURTHPi = PiDen.Clone()
        denFOURTHPi.Multiply(PiDen)
        denFOURTHPi.Multiply(PiDen)
        denFOURTHPi.Multiply(PiDen)
        
        ###divide
        betaPi.Divide(denFOURTHPi)
        #*4
        AsymmPiErrorSQUARE = betaPi.Clone()
        AsymmPiErrorSQUARE.Add(betaPi)
        AsymmPiErrorSQUARE.Add(betaPi)
        AsymmPiErrorSQUARE.Add(betaPi)
        
        ######CALCULATION FOR CORRELATION CO-EFFICIENT######
        DMuQuadSum = 0;
        DPiQuadSum = 0;
        DMuPiSum = 0;
        ###correlation coefficient calculated
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                DMuQuadSum = DMuQuadSum + DMuQuadName.GetBinContent(i,j);
                DPiQuadSum = DPiQuadSum + DPiQuadName.GetBinContent(i,j);
                DMuPiSum = DMuPiSum + DMuName.GetBinContent(i,j)*DPiName.GetBinContent(i,j);
        rho = [];
        rho.append(1)
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(1)
        ###deltaAMu and deltaAPi calculated
        deltaAMuSquare = 0;
        deltaAPiSquare = 0;
        for i in xrange(sliceNumber+1):
            for j in xrange(sliceNumber+1):
                deltaAMuSquare =  deltaAMuSquare + SigKinMu.GetBinContent(i,j)*AsymmMuErrorSQUARE.GetBinContent(i,j)
                deltaAPiSquare =  deltaAPiSquare + SigKinMu.GetBinContent(i,j)*AsymmPiErrorSQUARE.GetBinContent(i,j)

        deltaAMu = np.sqrt(deltaAMuSquare)/nSigMu
        deltaAPi = np.sqrt(deltaAPiSquare)/nSigPi
        
        ###covariance matrix and inverse calculated
        V = []
        V.append(rho[0]* deltaAMu *  deltaAMu)
        V.append(rho[1]* deltaAMu *  deltaAPi)
        V.append(rho[2]* deltaAPi *  deltaAMu)
        V.append(rho[3]* deltaAPi *  deltaAPi)

       
        Vdet = V[0]*V[3]-V[1]*V[2];
       
        Vinverse = [];
        Vinverse.append(V[3]/Vdet)
        Vinverse.append(-V[1]/Vdet)
        Vinverse.append(-V[2]/Vdet)
        Vinverse.append(V[0]/Vdet)
       
        VinverseSum = 0;
        VinverseMuSum = Vinverse[0]+Vinverse[1];
        VinversePiSum = Vinverse[2]+Vinverse[3];    
       
        for i in range(0,3):
            VinverseSum = VinverseSum + Vinverse[i];
       
        Omega=[]
                   
        Omega.append(VinverseMuSum/VinverseSum)
        Omega.append(VinversePiSum/VinverseSum)
       
        DeltaAMuPiTrack = np.sqrt(Omega[0]*Omega[0]*V[0] + Omega[0]*Omega[1]*V[1] + Omega[1]*Omega[0]*V[2] + Omega[1]*Omega[1]*V[3])
        if (DeltaAMuPiTrack < 0):
           DeltaAMuPiTrack = -DeltaAMuPiTrack;
           
        ###final values obtained by summing over bins
        efficiencyMuPlus = 0;
        efficiencyPiPlus = 0;
        efficiencyMuMinus = 0;
        efficiencyPiMinus = 0;
        errorEMuPlusSQUARE = 0;
        errorEMuMinusSQUARE = 0;
        errorEPiPlusSQUARE = 0;
        errorEPiMinusSQUARE = 0;        
        MuAsymm = 0
        errorMuAsymm = 0
        PiAsymm = 0;
        errorPiAsymm = 0;
        TotalAsymm = 0;
        GlobalErrorAsymmSQUARE=0;

        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                efficiencyMuPlus =efficiencyMuPlus + wMuPlus .GetBinContent(i,j)
                efficiencyMuMinus = efficiencyMuMinus + wMuMinus .GetBinContent(i,j)
                efficiencyPiPlus = efficiencyPiPlus + wPiPlus .GetBinContent(i,j)
                efficiencyPiMinus = efficiencyPiMinus + wPiMinus .GetBinContent(i,j)
                errorEMuPlusSQUARE =  errorEMuPlusSQUARE + errorWMuPlusEffSQUARED.GetBinContent(i,j)
                errorEMuMinusSQUARE = errorEMuMinusSQUARE + errorWMuMinusEffSQUARED.GetBinContent(i,j)
                errorEPiPlusSQUARE = errorEPiPlusSQUARE + errorWPiPlusEffSQUARED.GetBinContent(i,j)
                errorEPiMinusSQUARE = errorEPiMinusSQUARE + errorWPiMinusEffSQUARED.GetBinContent(i,j)
                MuAsymm =  MuAsymm  + AsymmMu.GetBinContent(i,j)
                errorMuAsymm = errorMuAsymm + AsymmMuErrorSQUARE.GetBinContent(i,j)
                PiAsymm = PiAsymm + AsymmPi.GetBinContent(i,j)
                errorPiAsymm = errorPiAsymm + AsymmPiErrorSQUARE.GetBinContent(i,j)
                TotalAsymm = TotalAsymm + AsymmTot.GetBinContent(i,j)
                GlobalErrorAsymmSQUARE = GlobalErrorAsymmSQUARE + AsymmErrorSQUARE.GetBinContent(i,j)
        #append values to lists for data frame
        if (systematic == True):
            PTlowLSRS.append(PTlow)
            efficiencyMuPlusLSRS.append(efficiencyMuPlus)
            efficiencyMuMinusLSRS.append(efficiencyMuMinus)
            errorEMuPlusLSRS.append(np.sqrt(errorEMuPlusSQUARE))
            errorEMuMinusLSRS.append(np.sqrt(errorEMuMinusSQUARE))
            efficiencyPiPlusLSRS.append(efficiencyPiPlus)
            efficiencyPiMinusLSRS.append(efficiencyPiMinus)
            errorEPiPlusLSRS.append(np.sqrt(errorEPiPlusSQUARE))
            errorEPiMinusLSRS.append(np.sqrt(errorEPiMinusSQUARE))
            GlobalAsymmLSRS.append(TotalAsymm*100)
            GlobalAsymmELSRS.append(np.sqrt(GlobalErrorAsymmSQUARE)*100)
            MuAsymmLSRS.append(MuAsymm*100)
            MuAsymmELSRS.append(np.sqrt(errorMuAsymm)*100)
            PiAsymmELSRS.append(np.sqrt(errorPiAsymm)*100)
            PiAsymmLSRS.append(PiAsymm*100)
            GlobalAsymmCorrErrSRS.append(DeltaAMuPiTrack*100)            
        else:
            PTlowLSR.append(PTlow)
            efficiencyMuPlusLSR.append(efficiencyMuPlus)
            efficiencyMuMinusLSR.append(efficiencyMuMinus)
            errorEMuPlusLSR.append(np.sqrt(errorEMuPlusSQUARE))
            errorEMuMinusLSR.append(np.sqrt(errorEMuMinusSQUARE))
            efficiencyPiPlusLSR.append(efficiencyPiPlus)
            efficiencyPiMinusLSR.append(efficiencyPiMinus)
            errorEPiPlusLSR.append(np.sqrt(errorEPiPlusSQUARE))
            errorEPiMinusLSR.append(np.sqrt(errorEPiMinusSQUARE))
            GlobalAsymmLSR.append(TotalAsymm*100)
            GlobalAsymmELSR.append(np.sqrt(GlobalErrorAsymmSQUARE)*100)
            MuAsymmLSR.append(MuAsymm*100)
            MuAsymmELSR.append(np.sqrt(errorMuAsymm)*100)
            PiAsymmELSR.append(np.sqrt(errorPiAsymm)*100)
            PiAsymmLSR.append(PiAsymm*100)
            GlobalAsymmCorrErrSR.append(DeltaAMuPiTrack*100)
       
    PTSlicesD = json.load( open( "PTSlicesD" + polarity + ".json" ) )
    ###run for all systematic and original PT slices
    for i in xrange(sliceNumber):
        SepReweightTwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTSlicesD["PTlow"+str(i)], home, sliceNumber, False)
    for i in xrange(sliceNumberSyst):    
        SepReweightTwoDHistSlices(DHistsSyst, JPlusHistsSyst, JMinusHistsSyst, polarity, PTSlicesDSyst["PTlow"+str(i)], home, sliceNumberSyst, True)
    ###save data as a csv
    dataSR = {"$PT low (MeV)$":PTlowL,
            "$Efficiency sig Mu Plus$" :efficiencyMuPlusLSR,
            "$Error Efficiencty sig Mu Plus$": errorEMuPlusLSR,
            "$Efficiency sig Mu Minus$" :efficiencyMuMinusLSR,
            "$Error Efficieny sig Mu Minus$":errorEMuMinusLSR,
            "$Efficiency sig Pi Plus$" :efficiencyPiPlusLSR,
            "$Error Efficiency sig Pi Plus$": errorEPiPlusLSR,
            "$Efficiency sig Pi Minus$" :efficiencyPiMinusLSR,
            "$Error Efficieny sig Pi Minus$":errorEPiMinusLSR,
            "$Mu Asymmetry (percent)$": MuAsymmLSR,
            "$Mu Asymm error (percent)$": MuAsymmELSR,
            "$Pi Asymmetry (percent)$": PiAsymmLSR,
            "$Pi Asymm error (percent)$": PiAsymmELSR,
            "$Global Asymmetry (percent)$": GlobalAsymmLSR,
            "$Global Asymmetry Error (percent)$": GlobalAsymmCorrErrSR
            }
    dfSR=pd.DataFrame(dataSR)
    column_orderSR = ["$PT low (MeV)$",
            "$Efficiency sig Mu Plus$",
            "$Error Efficiencty sig Mu Plus$",
            "$Efficiency sig Mu Minus$",
            "$Error Efficieny sig Mu Minus$",
            "$Efficiency sig Pi Plus$",
            "$Error Efficiency sig Pi Plus$",
            "$Efficiency sig Pi Minus$",
            "$Error Efficieny sig Pi Minus$",
            "$Mu Asymmetry (percent)$",
            "$Mu Asymm error (percent)$",
            "$Pi Asymmetry (percent)$",
            "$Pi Asymm error (percent)$",
            "$Global Asymmetry (percent)$",
            "$Global Asymmetry Error (percent)$"]
    dfSR[column_orderSR].to_csv('SepReWeight2DSlicesdata'+ polarity + '.csv', index = None)

    dataSRS = {"$PT low (MeV)$":PTlowLS,
            "$Efficiency sig Mu Plus$" :efficiencyMuPlusLSRS,
            "$Error Efficiencty sig Mu Plus$": errorEMuPlusLSRS,
            "$Efficiency sig Mu Minus$" :efficiencyMuMinusLSRS,
            "$Error Efficieny sig Mu Minus$":errorEMuMinusLSRS,
            "$Efficiency sig Pi Plus$" :efficiencyPiPlusLSRS,
            "$Error Efficiency sig Pi Plus$": errorEPiPlusLSRS,
            "$Efficiency sig Pi Minus$" :efficiencyPiMinusLSRS,
            "$Error Efficieny sig Pi Minus$":errorEPiMinusLSRS,
            "$Mu Asymmetry (percent)$": MuAsymmLSRS,
            "$Mu Asymm error (percent)$": MuAsymmELSRS,
            "$Pi Asymmetry (percent)$": PiAsymmLSRS,
            "$Pi Asymm error (percent)$": PiAsymmELSRS,
            "$Global Asymmetry (percent)$": GlobalAsymmLSRS,
            "$Global Asymmetry Error (percent)$": GlobalAsymmCorrErrSRS
            }
    dfSRS=pd.DataFrame(dataSRS)
    column_orderSRS = ["$PT low (MeV)$",
            "$Efficiency sig Mu Plus$",
            "$Error Efficiencty sig Mu Plus$",
            "$Efficiency sig Mu Minus$",
            "$Error Efficieny sig Mu Minus$",
            "$Efficiency sig Pi Plus$",
            "$Error Efficiency sig Pi Plus$",
            "$Efficiency sig Pi Minus$",
            "$Error Efficieny sig Pi Minus$",
            "$Mu Asymmetry (percent)$",
            "$Mu Asymm error (percent)$",
            "$Pi Asymmetry (percent)$",
            "$Pi Asymm error (percent)$",
            "$Global Asymmetry (percent)$",
            "$Global Asymmetry Error (percent)$"]
    dfSRS[column_orderSRS].to_csv('SepReWeight2DSlicesdata'+ polarity + 'Syst.csv', index = None)


    ###values for difference reweighting
    PTlowLDR=[]
    efficiencyPlusLDR=[]
    efficiencyMinusLDR=[]
    errorEPlusSQUARELDR=[]
    errorEMinusSQUARELDR=[]
    AsymmJLDR=[]
    errorAsymmLDR=[]
    GlobalAsymmLDR=[]
    GlobalAsymmELDR=[]
    GlobalAsymmECLDR=[]
    MuAsymmLDR=[]
    MuAsymmELDR=[]
    PiAsymmLDR=[]
    PiAsymmELDR=[]
    CorrErrLDR = []


    PTlowLDRS=[]
    efficiencyPlusLDRS=[]
    efficiencyMinusLDRS=[]
    errorEPlusSQUARELDRS=[]
    errorEMinusSQUARELDRS=[]
    AsymmJLDRS=[]
    errorAsymmLDRS=[]
    GlobalAsymmLDRS=[]
    GlobalAsymmELDRS=[]
    GlobalAsymmECLDRS=[]
    MuAsymmLDRS=[]
    MuAsymmELDRS=[]
    PiAsymmLDRS=[]
    PiAsymmELDRS=[]
    CorrErrLDRS = []
    
    def ReWeightTwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTlow,  home, sliceNumber, systematic):
        file1 = TFile.Open(DHists)
        file2 = TFile.Open(JPlusHists)
        file3 = TFile.Open(JMinusHists)
        
        DMuName = TH2F();
        file1.GetObject("DMu2D"+polarity+str(PTlow), DMuName);

        DMuQUADName = TH2F();
        file1.GetObject("DMuQUAD2D"+polarity+str(PTlow), DMuQUADName);        
       
        DPiName = TH2F();
        file1.GetObject("DPi2D"+polarity+str(PTlow), DPiName);

        DPiQUADName = TH2F();
        file1.GetObject("DPiQUAD2D"+polarity+str(PTlow), DPiQUADName);    
       
        JMinusName = TH2F();
        file2.GetObject("JPsiKin"+ polarity + "Plus"+ str(PTlow),JMinusName);
       
        JPlusName = TH2F();
        file3.GetObject("JPsiKin"+ polarity + "Minus" + str(PTlow), JPlusName);
       
        JMinusAssocName = TH2F();
        file2.GetObject("JPsiKinAssoc"+ polarity + "Plus"+ str(PTlow),JMinusAssocName);
       
        JPlusAssocName = TH2F();
        file3.GetObject("JPsiKinAssoc"+ polarity + "Minus" + str(PTlow),JPlusAssocName);

        fout.cd();


        ###Full J/Psi kin plus and minus events
        JPsiKinPlus = JPlusName.Clone();
        JPsiKinMinus = JMinusName.Clone();

        ###Matched J/Psi kin plus and minus events
        JPsiKinMinusAssoc = JMinusAssocName.Clone();
        JPsiKinPlusAssoc =  JPlusAssocName.Clone();
       
        SigKinMu = DMuName.Clone();
        SigKinMu.Multiply(DMuName);
        SigKinMu.Divide(DMuQUADName)

        SigKinPi = DPiName.Clone();
        SigKinPi.Multiply(DPiName);
        SigKinPi.Divide(DPiQUADName);

        TotSigMu = 0;
        TotSigPi = 0;
       
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                    TotSigMu = TotSigMu + SigKinMu.GetBinContent(i,j);
                    TotSigPi = TotSigPi + SigKinPi.GetBinContent(i,j);
                   
        TotSigKinMu = SigKinMu.Clone();
        TotSigKinPi = SigKinPi.Clone();
        OneHist = SigKinPi.Clone();
        OneHist.SetName("one");
        TwoHist = SigKinPi.Clone()
        TwoHist.SetName("TwoHist")
        OneHundredHist = SigKinPi.Clone();
        OneHundredHist.SetName("oneHundred");
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                    TotSigKinMu.SetBinContent(i,j, TotSigMu);
                    TotSigKinPi.SetBinContent(i,j, TotSigPi);
                    OneHist.SetBinContent(i,j, 1);
                    OneHundredHist.SetBinContent(i,j, 100);
                    TwoHist.SetBinContent(i,j,2)

                   
        SigKinMuProb = SigKinMu.Clone();
        SigKinMuProb.Divide(TotSigKinMu);
        SigKinMuProb.SetName("SigKinMuProb");
       
        SigKinPiProb = SigKinPi.Clone();
        SigKinPiProb.Divide(TotSigKinPi);
        SigKinPiProb.SetName("SigKinPiProb");
       
        SigKinDifProb = SigKinMuProb.Clone();
        SigKinDifProb.Add(SigKinPiProb, -1);
        SigKinDifProb.SetName("SigKinDifProb");
       
        ###calculate efficiencies by clone associated J/Psi events and dividing by total number of events
        efficiencyMuPlus = JPsiKinPlusAssoc.Clone();
        efficiencyMuMinus = JPsiKinMinusAssoc.Clone();
   
        efficiencyMuPlus.SetName("efficiencyMuPlus");
        efficiencyMuMinus.SetName("efficiencyMuMinus");
   
        efficiencyMuPlus.Divide(JPsiKinPlus);
        efficiencyMuMinus.Divide(JPsiKinMinus);
       
        efficiencyMuPlus.SetTitle("MuPlus Efficiency Control");
        efficiencyMuMinus.SetTitle("MuMinus Efficiency Control");
   
        (efficiencyMuPlus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuPlus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuPlus.GetYaxis()).SetTitle("\eta");
        efficiencyMuPlus.SetOption("BOX2Z");
   
        (efficiencyMuMinus.GetXaxis()).SetTitleOffset(1.5)
        (efficiencyMuMinus.GetXaxis()).SetTitle("\Phi");
        (efficiencyMuMinus.GetYaxis()).SetTitle("\eta");
        efficiencyMuMinus.SetOption("BOX2Z");
   
        efficiencyMuPlus.Write();
        efficiencyMuMinus.Write();

        ###asymmetry calculation
        ###calculate nominator and denominator of efficiency equation
        nom = efficiencyMuPlus.Clone();
        den = efficiencyMuPlus.Clone();
   
        nom.Add(efficiencyMuMinus, -1);
        den.Add(efficiencyMuMinus);
        """
        #weighted effiencies and asymmetries
        weight = SigKinMu.Clone()
        weight.Add(SigKinPi.Clone(), -1)
        weight.Divide(JPsiKinPlusAssoc)
        
        """
        ###difference reweighting weight
        weight = SigKinDifProb.Clone()
        weight.Divide(den)
        weight.Divide(TwoHist)
        
        sumWeight = 0
        sumWeightHist = weight.Clone()
        ###normalisation fo weight
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                if weight.GetBinContent(i,j) <0:
                    weight.SetBinContent(i,j,-weight.GetBinContent(i,j))
                sumWeight = sumWeight + weight.GetBinContent(i,j)
        
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                sumWeightHist.SetBinContent(i,j,sumWeight)
       
        
        
        ###weighted efficiencies calculated
        efficiencyMuPlusWeighted = efficiencyMuPlus.Clone()
        efficiencyMuPlusWeighted.Multiply(weight)
        efficiencyMuPlusWeighted.Divide(sumWeightHist)
       
        efficiencyMuMinusWeighted = efficiencyMuMinus.Clone()
        efficiencyMuMinusWeighted.Multiply(weight)
        efficiencyMuMinusWeighted.Divide(sumWeightHist)
        ###weighted muon tag and probe asymmetry calculated
        Wnom = efficiencyMuPlusWeighted.Clone()
        Wnom.Add(efficiencyMuMinusWeighted, -1)
       
        Wden=efficiencyMuPlusWeighted.Clone()
        Wden.Add(efficiencyMuMinusWeighted, +1)
       
        WAsymm=Wnom.Clone()
        WAsymm.Divide(Wden);
       
        WMuAsymm=WAsymm.Clone()
        WMuAsymm.Multiply(SigKinMuProb)
       
        WPiAsymm=WAsymm.Clone()
        WPiAsymm.Multiply(SigKinPiProb)

        WAsymmTot=WMuAsymm.Clone()
        WAsymmTot.Add(WPiAsymm, -1)

        TotalAsymm = 0;
        TotalAsymmMu = 0;
        TotalAsymmPi = 0;
        TotalPiProb = 0;
        TotalMuProb = 0;
        AsymmJ = 0;
        efficiencyPlus = 0;
        efficiencyMinus = 0;
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                efficiencyPlus = efficiencyPlus + efficiencyMuPlusWeighted.GetBinContent(i,j);
                efficiencyMinus = efficiencyMinus + efficiencyMuMinusWeighted.GetBinContent(i,j);
                AsymmJ= AsymmJ + WAsymm.GetBinContent(i,j)
                TotalAsymm = TotalAsymm + WAsymmTot.GetBinContent(i,j)
                TotalAsymmMu = TotalAsymmMu + WMuAsymm.GetBinContent(i,j)
                TotalAsymmPi = TotalAsymmPi + WPiAsymm.GetBinContent(i,j)
                TotalPiProb = TotalPiProb + SigKinPiProb.GetBinContent(i,j)
                TotalMuProb = TotalMuProb + SigKinMuProb.GetBinContent(i,j)
       
        #error propagation
        #error on weight
        #error weight nom
        #calculate error on total PDFs
        ProbPionErrorSQUARE = OneHist.Clone();
        ProbPionErrorSQUARE.Add(SigKinPiProb,-1);
        ProbPionErrorSQUARE.Multiply(SigKinPiProb);
        ProbPionErrorSQUARE.Divide(SigKinPi);

        ProbMuErrorSQUARE = OneHist.Clone();
        ProbMuErrorSQUARE.Add(SigKinMuProb,-1);
        ProbMuErrorSQUARE.Multiply(SigKinMuProb);
        ProbMuErrorSQUARE.Divide(SigKinMu);

        PErrorTotSQUARE=ProbPionErrorSQUARE.Clone()
        PErrorTotSQUARE.Add(ProbMuErrorSQUARE)
       
        #error propagation using binomial for efficiencies
        #efficiencyMuWeightedErrorSquare=eefficiencyMu(1-efficiencyMu)sumsWeightMu
        #muplus
        X=OneHist.Clone()
        X.Add(efficiencyMuPlus, -1)
        efficiencyMuPlusErrorSQUARE=efficiencyMuPlus.Clone()
        efficiencyMuPlusErrorSQUARE.Multiply(X)
        efficiencyMuPlusErrorSQUARE.Divide(JPsiKinPlus)
       
        #muminus
        Y=OneHist.Clone()
        Y.Add(efficiencyMuMinus, -1)
        efficiencyMuMinusErrorSQUARE=efficiencyMuMinus.Clone()
        efficiencyMuMinusErrorSQUARE.Multiply(Y)
        efficiencyMuMinusErrorSQUARE.Divide(JPsiKinMinus)

        #error on weight denominator
        ErrorWeightDenSQUARE=efficiencyMuPlusErrorSQUARE.Clone()
        ErrorWeightDenSQUARE.Add(efficiencyMuMinusErrorSQUARE)
        ErrorWeightDenSQUARE.Divide(TwoHist)
        ErrorWeightDenSQUARE.Divide(TwoHist)
        """
        #error weight total = enom^2/den^2 + eden^2*nom^2/den^4= eweight1 + e w2
        """
        ErrorWeight1SQUARE=PErrorTotSQUARE.Clone()
        ErrorWeight1SQUARE.Divide(den)
        ErrorWeight1SQUARE.Divide(den)
        ErrorWeight1SQUARE.Divide(TwoHist)
        ErrorWeight1SQUARE.Divide(TwoHist)
       
        ErrorWeight2SQUARE=ErrorWeightDenSQUARE.Clone()
        ErrorWeight2SQUARE.Divide(den)
        ErrorWeight2SQUARE.Divide(den)
        ErrorWeight2SQUARE.Divide(den)
        ErrorWeight2SQUARE.Divide(den)
        ErrorWeight2SQUARE.Divide(TwoHist)
        ErrorWeight2SQUARE.Divide(TwoHist)
        ErrorWeight2SQUARE.Divide(TwoHist)
        ErrorWeight2SQUARE.Divide(TwoHist)
        ErrorWeight2SQUARE.Multiply(SigKinDifProb)
        ErrorWeight2SQUARE.Multiply(SigKinDifProb)
       
        ErrorWeightSQUARE=ErrorWeight1SQUARE.Clone()
        ErrorWeightSQUARE.Add(ErrorWeight2SQUARE)
        
        sumWeightErrorSQUARE = 0;
        for i in xrange(sliceNumber + 1):
            sumWeightErrorSQUARE = sumWeightErrorSQUARE + ErrorWeightSQUARE.GetBinContent(i,j)
        sumWeightErrorSQUAREHist = ErrorWeightSQUARE.Clone();
        for i in xrange(sliceNumber + 1):
            sumWeightErrorSQUAREHist.SetBinContent(i,j,sumWeightErrorSQUARE);

        ErrorWeightSQUARE.Multiply(sumWeightHist);
        ErrorWeightSQUARE.Multiply(sumWeightHist);
        
        BetaErrorWeightSQUARE = sumWeightErrorSQUAREHist.Clone();
        BetaErrorWeightSQUARE.Multiply(weight);
        BetaErrorWeightSQUARE.Multiply(weight);

        #error weighted asymmetry
        #psi=((we+ - we-)/(we+ + we-)= (A-B/A+B)
        A1errorSQUARE=efficiencyMuPlusErrorSQUARE.Clone()
        A1errorSQUARE.Multiply(weight)
        A1errorSQUARE.Multiply(weight)
       
        A2errorSQUARE=ErrorWeightSQUARE.Clone()
        A2errorSQUARE.Multiply(efficiencyMuPlus)
        A2errorSQUARE.Multiply(efficiencyMuPlus)
       
        AerrorSQUARE= A1errorSQUARE.Clone()
        AerrorSQUARE.Add(A2errorSQUARE)
       
        B1errorSQUARE=efficiencyMuMinusErrorSQUARE.Clone()        
        B1errorSQUARE.Multiply(weight)
        B1errorSQUARE.Multiply(weight)
       
        B2errorSQUARE=ErrorWeightSQUARE.Clone()
        B2errorSQUARE.Multiply(efficiencyMuMinus)
        B2errorSQUARE.Multiply(efficiencyMuMinus)
       
        BerrorSQUARE= B1errorSQUARE.Clone()
        BerrorSQUARE.Add(B2errorSQUARE)
       
        #calculating asuymmety errors
        #error Asymm^2=4((A^2*sig_B^2 + B^2*sigA^2)/(A + B)^4)
        #first bit nom
        beta = efficiencyMuPlusWeighted.Clone()
        beta.Multiply(efficiencyMuPlusWeighted)
        beta.Multiply(BerrorSQUARE)
        #second bit nom
        gamma = efficiencyMuMinusWeighted.Clone()
        gamma.Multiply(efficiencyMuMinusWeighted)
        gamma.Multiply(AerrorSQUARE)
        #add
        beta.Add(gamma)
        #den
        WdenFOURTH = Wden.Clone()
        WdenFOURTH.Multiply(Wden)
        WdenFOURTH.Multiply(Wden)
        WdenFOURTH.Multiply(Wden)
        #divide
        beta.Divide(WdenFOURTH)
        #*4
        WAsymmErrorSQUARE = beta.Clone()
        WAsymmErrorSQUARE.Add(beta)
        WAsymmErrorSQUARE.Add(beta)
        WAsymmErrorSQUARE.Add(beta)      
        
       
        GlobalWErrorAsymmSQUARE = 0;
        TotalJPsiError = 0;
        TotalMuError = 0;
        TotalPiError = 0;
        sumOneHist = 0;
        WefficiencyPlusE=0
        WefficiencyMinusE=0
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                sumOneHist = sumOneHist + JPsiKinMinus.GetBinContent(i,j);
                TotalJPsiError = TotalJPsiError + WAsymmErrorSQUARE.GetBinContent(i,j);
                TotalMuError = TotalMuError + WAsymmMuError.GetBinContent(i,j);
                TotalPiError = TotalPiError + WAsymmPiError.GetBinContent(i,j);
                GlobalWErrorAsymmSQUARE = GlobalWErrorAsymmSQUARE + WAsymmTotErrorSquare.GetBinContent(i,j);
                WefficiencyPlusE=WefficiencyPlusE + AerrorSQUARE.GetBinContent(i,j)
                WefficiencyMinusE=WefficiencyMinusE + BerrorSQUARE.GetBinContent(i,j)
       
        if (GlobalWErrorAsymmSQUARE < 0):
            modGEA =  -GlobalWErrorAsymmSQUARE;
            GlobalWErrorAsymmSQUARE = modGEA;

        ######CALCULATION FOR CORRELATION CO-EFFICIENT######
        DMuQuadSum = 0;
        DPiQuadSum = 0;
        DMuPiSum = 0;
        ###correlation coeeficient calculated
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                DMuQuadSum = DMuQuadSum + DMuQUADName.GetBinContent(i,j);
                DPiQuadSum = DPiQuadSum + DPiQUADName.GetBinContent(i,j);
                DMuPiSum = DMuPiSum + DMuName.GetBinContent(i,j)*DPiName.GetBinContent(i,j);
        rho = [];
        rho.append(-1)
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(DMuPiSum/np.sqrt(DMuQuadSum*DPiQuadSum))
        rho.append(-1)
        ###deltaAMu and deltaAPi calculated
        deltaAMuSquare = 0;
        deltaAPiSquare = 0;
        for i in xrange(sliceNumber + 1):
            for j in xrange(sliceNumber + 1):
                deltaAMuSquare =  deltaAMuSquare + (DMuQUADName.GetBinContent(i,j)*WAsymmErrorSQUARE.GetBinContent(i,j))
                deltaAPiSquare =  deltaAPiSquare + (DPiQUADName.GetBinContent(i,j)*WAsymmErrorSQUARE.GetBinContent(i,j))
       
        deltaAMu = np.sqrt(deltaAMuSquare)/TotSigMu
        deltaAPi = np.sqrt(deltaAPiSquare)/TotSigPi    
        ###covariance matrix and inverse calculated
        V = []
        V.append(rho[0]* deltaAMu *  deltaAMu)
        V.append(rho[1]* deltaAMu *  deltaAPi)
        V.append(rho[2]* deltaAPi *  deltaAMu)
        V.append(rho[3]* deltaAPi *  deltaAPi)

       
        Vdet = V[0]*V[3]-V[1]*V[2];
       
        Vinverse = [];
        Vinverse.append(V[3]/Vdet)
        Vinverse.append(-V[1]/Vdet)
        Vinverse.append(-V[2]/Vdet)
        Vinverse.append(V[0]/Vdet)
       
        VinverseSum = 0;
        VinverseMuSum = Vinverse[0]+Vinverse[1];
        VinversePiSum = Vinverse[2]+Vinverse[3];    
       
        for i in range(0,3):
            VinverseSum = VinverseSum + Vinverse[i];
     
        Omega=[]
                   
        Omega.append(VinverseMuSum/VinverseSum)
        Omega.append(VinversePiSum/VinverseSum)
        ###tracking asymmetry error calculated  
        DeltaAMuPiTrack = np.sqrt(Omega[0]*Omega[0]*V[0] + Omega[0]*Omega[1]*V[1] + Omega[1]*Omega[0]*V[2] + Omega[1]*Omega[1]*V[3])
        if (DeltaAMuPiTrack < 0):
           DeltaAMuPiTrack = -DeltaAMuPiTrack;
        ###values apppeded to lists for data frames to be outputted as a csv
        if (systematic == True):
            PTlowLDRS.append(PTlow)
            efficiencyPlusLDRS.append(efficiencyPlus)
            efficiencyMinusLDRS.append(efficiencyMinus)
            errorEPlusSQUARELDRS.append(np.sqrt(WefficiencyPlusE))
            errorEMinusSQUARELDRS.append(np.sqrt(WefficiencyMinusE))
            AsymmJLDRS.append(AsymmJ*100)
            errorAsymmLDRS.append(np.sqrt(TotalJPsiError)*100)
            GlobalAsymmLDRS.append(TotalAsymm*100)
            GlobalAsymmELDRS.append(np.sqrt(GlobalWErrorAsymmSQUARE)*100)
            CorrErrLDRS.append(DeltaAMuPiTrack*100)
            MuAsymmLDRS.append(TotalAsymmMu*100)
            MuAsymmELDRS.append(TotalMuError*100)
            PiAsymmELDRS.append(TotalPiError*100)
            PiAsymmLDRS.append(TotalAsymmPi*100) 
            
        else:
            PTlowLDR.append(PTlow)
            efficiencyPlusLDR.append(efficiencyPlus)
            efficiencyMinusLDR.append(efficiencyMinus)
            errorEPlusSQUARELDR.append(np.sqrt(WefficiencyPlusE))
            errorEMinusSQUARELDR.append(np.sqrt(WefficiencyMinusE))
            AsymmJLDR.append(AsymmJ*100)
            errorAsymmLDR.append(np.sqrt(TotalJPsiError)*100)
            GlobalAsymmLDR.append(TotalAsymm*100)
            GlobalAsymmELDR.append(np.sqrt(GlobalWErrorAsymmSQUARE)*100)
            CorrErrLDR.append(DeltaAMuPiTrack*100)
            MuAsymmLDR.append(TotalAsymmMu*100)
            MuAsymmELDR.append(TotalMuError*100)
            PiAsymmELDR.append(TotalPiError*100)
            PiAsymmLDR.append(TotalAsymmPi*100) 

    PTSlicesD = json.load( open( "PTSlicesD" + polarity + ".json" ) )
    ###function called for all original and systematic PT slices
    for i in xrange(sliceNumber):
        ReWeightTwoDHistSlices(DHists, JPlusHists, JMinusHists, polarity, PTSlicesD["PTlow"+str(i)], home, sliceNumber, False)
    for i in xrange(sliceNumberSyst):
        ReWeightTwoDHistSlices(DHistsSyst, JPlusHistsSyst, JMinusHistsSyst, polarity, PTSlicesDSyst["PTlow"+str(i)], home, sliceNumberSyst, True)
    ###data outputted as a csv
    dataDR = {'$PTlow$':PTlowLDR,
            '$efficiencyPlus$' :efficiencyPlusLDR,
            '$Error Plus$': errorEPlusSQUARELDR,
            '$efficiencyMinus$' :efficiencyMinusLDR,
            '$Error Minus$':errorEMinusSQUARELDR,
            '$Asymmetry %$' : AsymmJLDR,
            '$error Asymmetry %$': errorAsymmLDR,
            '$Mu Asymmetry %$': MuAsymmLDR,
            '$Mu Asymm error %$': MuAsymmELDR,
            '$Pi Asymmetry %$': PiAsymmLDR,
            '$Pi Asymm error %$': PiAsymmELDR,
            '$Global Asymmetry %$': GlobalAsymmLDR,
            '$Global Asymmetry Error %$': GlobalAsymmELDR,
            '$Global Asymmetry Error Corr$' : CorrErrLDR
            }
    column_orderDR=['$PTlow$',
                  '$efficiencyPlus$',
                  '$Error Plus$',
                  '$efficiencyMinus$',
                  '$Error Minus$',
                  '$Asymmetry %$',
                  '$error Asymmetry %$',
                  '$Mu Asymmetry %$',
                  '$Mu Asymm error %$',
                  '$Pi Asymmetry %$',
                  '$Pi Asymm error %$',
                  '$Global Asymmetry %$',
                  '$Global Asymmetry Error %$',
                  '$Global Asymmetry Error Corr$']
    dfDR=pd.DataFrame(dataDR)
    dfDR[column_orderDR].to_csv('DifferenceReweighted'+ polarity + '.csv', index = None)
    
    dataDRS = {'$PTlow$':PTlowLDRS,
            '$efficiencyPlus$' :efficiencyPlusLDRS,
            '$Error Plus$': errorEPlusSQUARELDRS,
            '$efficiencyMinus$' :efficiencyMinusLDRS,
            '$Error Minus$':errorEMinusSQUARELDRS,
            '$Asymmetry %$' : AsymmJLDRS,
            '$error Asymmetry %$': errorAsymmLDRS,
            '$Mu Asymmetry %$': MuAsymmLDRS,
            '$Mu Asymm error %$': MuAsymmELDRS,
            '$Pi Asymmetry %$': PiAsymmLDRS,
            '$Pi Asymm error %$': PiAsymmELDRS,
            '$Global Asymmetry %$': GlobalAsymmLDRS,
            '$Global Asymmetry Error %$': GlobalAsymmELDRS,
            '$Global Asymmetry Error Corr$' : CorrErrLDRS
            }
    column_orderDRS=['$PTlow$',
                  '$efficiencyPlus$',
                  '$Error Plus$',
                  '$efficiencyMinus$',
                  '$Error Minus$',
                  '$Asymmetry %$',
                  '$error Asymmetry %$',
                  '$Mu Asymmetry %$',
                  '$Mu Asymm error %$',
                  '$Pi Asymmetry %$',
                  '$Pi Asymm error %$',
                  '$Global Asymmetry %$',
                  '$Global Asymmetry Error %$',
                  '$Global Asymmetry Error Corr$']
    dfDRS=pd.DataFrame(dataDRS)
    dfDRS[column_orderDRS].to_csv('DifferenceReweighted'+ polarity + 'Syst.csv', index = None)
    
    
    #call dictionary
    PTSlicesA = [1.000]*(sliceNumber+1)
    
    for i in range(0,sliceNumber+1):
        PTSlicesA[i] = (PTSlicesD["PTlow"+str(i)]/1000)
        

    #call dictionary
    PTSlicesASyst = [1.000]*(sliceNumberSyst+1)
    
    for i in range(0,sliceNumberSyst+1):
        PTSlicesASyst[i] = (PTSlicesDSyst["PTlow"+str(i)]/1000)
        
    #create histograms for final asymmetry values
    AsymmPB = TH1F("PTValues", "PTValues", sliceNumber, array('f',PTSlicesA))
    AsymmSR = TH1F("PTValuesSR", "PTValuesSR", sliceNumber, array('f',PTSlicesA))
    AsymmDR = TH1F("PTValuesDR", "PTValuesDR", sliceNumber, array('f',PTSlicesA))

    #create histograms for final asymmetry values
    AsymmPBS = TH1F("PTValuesS", "PTValuesS", sliceNumberSyst, array('f',PTSlicesASyst))
    AsymmSRS = TH1F("PTValuesSRS", "PTValuesSRS", sliceNumberSyst, array('f',PTSlicesASyst))
    AsymmDRS = TH1F("PTValuesDRS", "PTValuesDRS", sliceNumberSyst, array('f',PTSlicesASyst))
    
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices
    weightedAsymm = [];
    errorWeightedAsymmStat = [];
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the original phase space binning method    
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumber):
        AsymmPB.SetBinContent(i+1,GlobalAsymmL[i])
        AsymmPB.SetBinError(i+1,CorrErr[i])
        sumWeights = sumWeights +  1/(CorrErr[i]*CorrErr[i])
        sumWeightedValue = sumWeightedValue + GlobalAsymmL[i]/(CorrErr[i]*CorrErr[i])
    wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(1/sumWeights))
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the systematic phase space binning method    
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumberSyst):
        AsymmPBS.SetBinContent(i+1,GlobalAsymmLS[i])
        AsymmPBS.SetBinError(i+1,CorrErrS[i])
        sumWeights = sumWeights +  1/(CorrErrS[i]*CorrErrS[i])
        sumWeightedValue = sumWeightedValue + GlobalAsymmLS[i]/(CorrErrS[i]*CorrErrS[i])
    wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(1/sumWeights))
    print(wMean)
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the separate reweighting method    
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumber):
        AsymmSR.SetBinContent(i+1,GlobalAsymmLSR[i])
        AsymmSR.SetBinError(i+1,GlobalAsymmCorrErrSR[i])
        sumWeights = sumWeights + GlobalAsymmCorrErrSRS[i]*GlobalAsymmCorrErrSRS[i]
        sumWeightedValue = sumWeightedValue + GlobalAsymmLSRS[i]      
    wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(sumWeights))    
    print(wMean)
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the systematic separate reweighting method    
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumberSyst):
        AsymmSRS.SetBinContent(i+1,GlobalAsymmLSRS[i])
        AsymmSRS.SetBinError(i+1,GlobalAsymmCorrErrSRS[i])
        sumWeights = sumWeights + GlobalAsymmCorrErrSRS[i]*GlobalAsymmCorrErrSRS[i]
        sumWeightedValue = sumWeightedValue + GlobalAsymmLSRS[i] 
    wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(sumWeights))   
    print(wMean)
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the difference reweighting method    
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumber):
        AsymmDR.SetBinContent(i+1,GlobalAsymmLDR[i])
        AsymmDR.SetBinError(i+1,CorrErrLDR[i])
        sumWeights = sumWeights + 1/(CorrErrLDR[i]*CorrErrLDR[i])
        sumWeightedValue = sumWeightedValue + GlobalAsymmLDR[i]/(CorrErrLDR[i]*CorrErrLDR[i]) 
    wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(1/sumWeights))
    print(wMean)
    ###calculated global weighted average for each method aswell as filling histograms
    ###with values and errors from PT slices for the systematic difference reweighting method   
    sumWeights = 0;
    sumWeightedValue = 0;
    for i in xrange(sliceNumberSyst):
        AsymmDRS.SetBinContent(i+1,GlobalAsymmLDRS[i])
        AsymmDRS.SetBinError(i+1,CorrErrLDRS[i])
        sumWeights = sumWeights + 1/(CorrErrLDRS[i]*CorrErrLDRS[i])
        sumWeightedValue = sumWeightedValue + GlobalAsymmLDRS[i]/(CorrErrLDRS[i]*CorrErrLDRS[i]) 
        wMean = sumWeightedValue/sumWeights;
    weightedAsymm.append(wMean);
    errorWeightedAsymmStat.append(np.sqrt(1/sumWeights))
    print(wMean)

    ###plots histograms of results for original PT slices with te results from different 
    ###methods overlaid
    e  = TCanvas("AsymmPB", "AsymmPB", 0, 650, 600, 500);
    pad = TPad("pad", "pad", 0.03, 0.03, 1., 1.)
    pad.Draw()
    pad.cd()
    
    AsymmDR.SetTitle("");
    AsymmDR.SetTitle("");
    AsymmDR.SetLineWidth(2);
    #AsymmDR.GetXaxis().SetRangeUser(0, 10);
    (AsymmDR.GetXaxis()).SetTitle("p_{T}[GeV]")
    (AsymmDR.GetXaxis()).SetTitleOffset(1.1)
    (AsymmDR.GetYaxis()).SetTitle("Asymm[%]")
    (AsymmDR.GetXaxis()).SetTitleSize(0.05)
    (AsymmDR.GetYaxis()).SetTitleSize(0.05)
    (AsymmDR.GetXaxis()).SetLabelSize(0.05)
    (AsymmDR.GetYaxis()).SetLabelSize(0.05)
    AsymmDR.Write()

    AsymmDR.Draw();

    e.Update()
    
    AsymmSR.SetMarkerColor(1);
    AsymmSR.SetLineColor(1);
    AsymmSR.SetLineWidth(1);
    AsymmSR.Draw("same");
    e.Update();
    AsymmPB.SetMarkerColor(2);
    AsymmPB.SetLineColor(2);
    AsymmPB.SetLineWidth(2);
    AsymmPB.Draw("same");
    e.Update();   
    
    leg = r.TLegend(0.55,0.66,0.883,0.875)  
    leg.SetFillColor(0)    
    leg.SetBorderSize(0)    
    leg.SetTextFont(132)
    leg.SetTextSize(0.049)
    leg.AddEntry(AsymmPB, "Phase binning")  
    leg.AddEntry(AsymmSR, "Sep. reweighting")
    leg.AddEntry(AsymmDR, "Dif. reweighting")  
    leg.AddEntry
    leg.Draw()

    leg.Draw()    
    e.Update()

    e.Write();
   
    e.SaveAs("GlobalAsymm" + polarity + ".png")
    ###plots histograms of results for systematic PT slices with te results from different 
    ###methods overlaid    
    f  = TCanvas("AsymmPBS", "AsymmPBS", 0, 650, 600, 500);
    pad1 = TPad("pad", "pad", 0.03, 0.03, 1., 1.)
    pad1.Draw()
    pad1.cd()
    
    AsymmDRS.SetTitle("");
    AsymmDRS.SetTitle("");
    AsymmDRS.SetLineWidth(2);
    #AsymmDR.GetXaxis().SetRangeUser(0, 10);
    (AsymmDRS.GetXaxis()).SetTitle("p_{T}[GeV]")
    (AsymmDRS.GetXaxis()).SetTitleOffset(1.1)
    (AsymmDRS.GetYaxis()).SetTitle("Asymm[%]")
    (AsymmDRS.GetXaxis()).SetTitleSize(0.05)
    (AsymmDRS.GetYaxis()).SetTitleSize(0.05)
    (AsymmDRS.GetXaxis()).SetLabelSize(0.05)
    (AsymmDRS.GetYaxis()).SetLabelSize(0.05)
    AsymmDRS.Write()

    AsymmDRS.Draw();

    f.Update()
    
    AsymmSRS.SetMarkerColor(1);
    AsymmSRS.SetLineColor(1);
    AsymmSRS.SetLineWidth(1);
    AsymmSRS.Draw("same");
    f.Update();
    AsymmPBS.SetMarkerColor(2);
    AsymmPBS.SetLineColor(2);
    AsymmPBS.SetLineWidth(2);
    AsymmPBS.Draw("same");
    f.Update();   
    
    leg = r.TLegend(0.55,0.66,0.883,0.875)    
    leg.SetFillColor(0)    
    leg.SetBorderSize(0)    
    leg.SetTextFont(132)
    leg.SetTextSize(0.049)
    leg.AddEntry(AsymmPBS, "Phase binning")  
    leg.AddEntry(AsymmSRS, "Sep. reweighting")
    leg.AddEntry(AsymmDRS, "Dif. reweighting")  
    leg.AddEntry
    leg.Draw()

    leg.Draw()    
    f.Update()

    f.Write();
   
    f.SaveAs("GlobalAsymm10" + polarity + ".png")

    ###save final PT slice values and errors to a csv
    dataFinal = {'$Asymmetry$' :weightedAsymm, 
            '$Stat Error$': errorWeightedAsymmStat,
            }
    column_orderFinal=['$Asymmetry$',
                  '$Stat Error$',
                  ]
    dfFinal=pd.DataFrame(dataFinal)
    dfFinal[column_orderFinal].to_csv('Final2DSlicesdata'+ polarity + '.csv', index = None) 
