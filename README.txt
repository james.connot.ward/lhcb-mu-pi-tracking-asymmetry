Code written colaboratively by James Ward and Jorden Wilder

INSTRUCTIONS ON HOW TO USE CODE:
IMPORTANT: This code must be ran in CERN's EOS space with permission to the LHCb gorup.

1) Merge trkcalib files from relevant location. NOTE (We are trying to change this step to utilise TCHAINS, but this is a work in progress)
    A copy of the 2016 Up merged control data can be found at: "/eos/lhcb/user/j/jward/LHCb2016Data/mergedtrkcalibUp""Plus" or "Minus".root
    
    This is done by running MergeTrees function
    (This is the longest step in running the code)
    
2) Run ObtainSWeightsDM function for the D data (location is stored in tree1)

    The location for the 2016 signal Up data is: "/eos/lhcb/user/l/liyu/asld_analysis/writeTuple/Data2016/RunII_Data2016_MagUp.root"
    The location for the 2016 signal Down data is: "/eos/lhcb/user/l/liyu/asld_analysis/writeTuple/Data2016/RunII_Data2016_MagDown.root"

3) Obtain bin limits and fill D histograms with matched and full probe tracks using the FillDHistAdapt function

4) Get Datasets broken down into PT slices containing data from JPsiPlus and Minus Tuples using GetDataSet function
       
    The location for the 2016 merged plus data is: "/eos/lhcb/user/j/jward/LHCb2016Data/mergedtrkcalibUpPlus.root"
    The location for the 2016 merged minus data is: "/eos/lhcb/user/j/jward/LHCb2016Data/mergedtrkcalibUpMinus.root"
    
5) Run storeSWeights function to obtain sWeights for the PT slices of the J/Psi tag and probe event

NOTE: 2 and 5 store sWeights in a text file in your home directory
NOTE: Fitting parameters may need to be changed in the storeSWeight function when running the code for different datasets

6) Run the fill histogram functions for the control tag and probe dataSet usin the adaptive bin limits
    
7) Run the plot_SignalAll, twoDHists and plot_1Dhist to get the appropriate graphs; 
    Details of the produced graphs are explained where the function is called

###Note the twoDHist function may contain bugs and the separate reweighting method needs finalising

Running the 2D Hist returns values for the asymmetry of the slices. These values can be used to calculate a weighted average.

Copies of all graphs in png format can be transfered from the code directory to a local machine using scp. This is possible on windows, linux and OS machines

If you encounter any issues or bugs please contact:
Jorden Wilder @: jorden.wilder@student.manchester.ac.uk
James Ward @: james.ward-6@student.manchester.ac.uk