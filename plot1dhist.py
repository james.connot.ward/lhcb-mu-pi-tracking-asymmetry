from ROOT import TChain, TFile, TTree, TBranch, TTreeReader, TGaxis, TH1F, TCanvas, TMath, TStyle, TH2F, TH3F, TGraph2D, TLegend, RooFormulaVar, RooDataSet, RooDataHist, RooCategory, RooKeysPdf, TLorentzVector, TList, gROOT, gDirectory, TAxis, RooPlot, RooHist, RooPlotable, RooCurve,TGLHistPainter, TPad, TGraph
import ROOT as r

import numpy as np
import os
import json
from array import array


def plot_1Dhist(JPlusHist, JMinusHist, output, home, polarity, sliceNumber):
    etalow=1
    etahigh=5
    philow=-3.2
    phihigh=3.14
    ptlow=0
    pthigh=7000
    import math
    r.gStyle.SetOptStat(0)
    r.gROOT.SetBatch(True)
    r.gROOT.ProcessLine(".x lhcbStyle.C")
    r.gROOT.ForceStyle()

    #opens the input file containing histograms
    file1 = TFile.Open(JPlusHist);
    file2= TFile.Open(JMinusHist);


    fout = r.TFile(output,'RECREATE');
    #histograms defined to store matched and full tracks 
    JPsiETAPlus = TH1F();
    JPsiETAPlusAssoc = TH1F();
    JPsiPTPlus = TH1F();
    JPsiPTPlusAssoc = TH1F();
    JPsiPHIPlus = TH1F();
    JPsiPHIPlusAssoc = TH1F();
    
    JPsiETAMinus = TH1F();
    JPsiETAMinusAssoc = TH1F();
    JPsiPTMinus = TH1F();
    JPsiPTMinusAssoc = TH1F();
    JPsiPHIMinus = TH1F();
    JPsiPHIMinusAssoc = TH1F();
    
    #histograms obtained from input file
    file1.GetObject("JPsiKinETA" + polarity + "Plus", JPsiETAMinus)
    file2.GetObject("JPsiKinETA" + polarity + "Minus", JPsiETAPlus)  
    file1.GetObject("JPsiKinPT" + polarity + "Plus", JPsiPTMinus)
    file2.GetObject("JPsiKinPT" + polarity + "Minus", JPsiPTPlus)  
    file1.GetObject("JPsiKinPHI" + polarity + "Plus", JPsiPHIMinus)
    file2.GetObject("JPsiKinPHI" + polarity + "Minus", JPsiPHIPlus) 
    
    file1.GetObject("JPsiKinETAAssoc" + polarity + "Plus", JPsiETAMinusAssoc)
    file2.GetObject("JPsiKinETAAssoc" + polarity + "Minus", JPsiETAPlusAssoc)  
    file1.GetObject("JPsiKinPTAssoc" + polarity + "Plus", JPsiPTMinusAssoc)
    file2.GetObject("JPsiKinPTAssoc" + polarity + "Minus", JPsiPTPlusAssoc)  
    file1.GetObject("JPsiKinPHIAssoc" + polarity + "Plus", JPsiPHIMinusAssoc)
    file2.GetObject("JPsiKinPHIAssoc" + polarity + "Minus", JPsiPHIPlusAssoc)
    
    PTSlicesD = json.load(open( "PTSlicesD"+polarity+".json" ))
    PHISlicesD = json.load(open( "PHISlicesD"+polarity+".json" ))
    ETASlicesD = json.load(open( "ETASlicesD"+polarity+".json" ))
    
    #load bin limits into an arrays
    PTSlicesA = [1.000]*(sliceNumber+1)
    PHISlicesA = [1.000]*(sliceNumber+1)
    ETASlicesA = [1.000]*(sliceNumber+1)
    for i in range(0,sliceNumber+1):
        #load PT limits in GeV
        PTSlicesA[i] = (PTSlicesD["PTlow"+str(i)])/1000
        PHISlicesA[i] = (PHISlicesD["PHIlow"+str(i)])
        ETASlicesA[i] = (ETASlicesD["ETAlow"+str(i)])
    AsymmPT = TH1F("AsymmPT", "AsymmPT", sliceNumber, array('f', PTSlicesA))
    AsymmPHI = TH1F("AsymmPHI", "AsymmPHI", sliceNumber, array('f', PHISlicesA))
    AsymmETA = TH1F("AsymmETA", "AsymmETA", sliceNumber, array('f', ETASlicesA))

    #calculate asymmetry value for every PT slice
    for k in range(0,sliceNumber):
        #histograms containing matched and full probe tracks obtained
        JPsiKinMinus = TH2F();
        file1.GetObject("JPsiKin"+ polarity + "Plus"+ str(PTSlicesD["PTlow"+str(k)]),JPsiKinMinus);
       
        JPsiKinPlus = TH2F();
        file2.GetObject("JPsiKin"+ polarity + "Minus" + str(PTSlicesD["PTlow"+str(k)]), JPsiKinPlus);
       
        JPsiKinMinusAssoc = TH2F();
        file1.GetObject("JPsiKinAssoc"+ polarity + "Plus"+ str(PTSlicesD["PTlow"+str(k)]),JPsiKinMinusAssoc);
       
        JPsiKinPlusAssoc = TH2F();
        file2.GetObject("JPsiKinAssoc"+ polarity + "Minus" + str(PTSlicesD["PTlow"+str(k)]),JPsiKinPlusAssoc);
        
        #other variables integrated over
        JPsiKinPlusTot = 0;
        JPsiKinPlusAssocTot = 0;
        JPsiKinMinusTot = 0;
        JPsiKinMinusAssocTot = 0;        
        for i in xrange(sliceNumber):
            for j in xrange(sliceNumber):
                JPsiKinPlusTot = JPsiKinPlusTot + JPsiKinPlus.GetBinContent(i,j);
                JPsiKinMinusTot = JPsiKinMinusTot + JPsiKinMinus.GetBinContent(i,j);
                JPsiKinPlusAssocTot = JPsiKinPlusAssocTot + JPsiKinPlusAssoc.GetBinContent(i,j);
                JPsiKinMinusAssocTot = JPsiKinMinusAssocTot + JPsiKinMinusAssoc.GetBinContent(i,j);
        #asymmetry and errors calculated
        efficiencyPlus = JPsiKinPlusAssocTot/JPsiKinPlusTot
        efficiencyMinus = JPsiKinMinusAssocTot/JPsiKinMinusTot      
        
        AsymmJ = (efficiencyPlus-efficiencyMinus)/(efficiencyPlus+efficiencyMinus);
        print(AsymmJ);
        errorEPlusSQUARE = efficiencyPlus*(1-efficiencyPlus)/JPsiKinPlusTot;
        errorEMinusSQUARE = efficiencyMinus*(1-efficiencyMinus)/JPsiKinMinusTot;

        errorNom = 4*(efficiencyPlus*efficiencyPlus*errorEMinusSQUARE + efficiencyMinus*efficiencyMinus*errorEPlusSQUARE);
        Beta = efficiencyPlus + efficiencyMinus;
        errorDen = Beta * Beta * Beta * Beta;
        errorAsymm = errorNom/errorDen
        #saved in a histogram to be plotted
        AsymmPT.SetBinContent(k+1,AsymmJ*100);
        AsymmPT.SetBinError(k+1, np.sqrt(errorAsymm)*100);
    #PT dependence plotted
    AsymmPT.SetTitle("");
    AsymmPT.SetLineWidth(2);
    AsymmPT.GetXaxis().SetRangeUser(0, 10);
    (AsymmPT.GetXaxis()).SetTitle("p_{T}[GeV]")
    (AsymmPT.GetXaxis()).SetTitleOffset(1.1)
    (AsymmPT.GetYaxis()).SetTitle("Asymm[%]")
    (AsymmPT.GetXaxis()).SetTitleSize(0.058)
    (AsymmPT.GetYaxis()).SetTitleSize(0.058)
    (AsymmPT.GetXaxis()).SetLabelSize(0.058)
    (AsymmPT.GetYaxis()).SetLabelSize(0.058)
    AsymmPT.Write()
    e  = TCanvas("AsymmPT", "AsymmPT", 0, 650, 750, 500);
    pad = TPad("pad", "pad", 0.03, 0.06, 0.97, 1.)
    pad.Draw()
    pad.cd()
    AsymmPT.Draw();
    e.Modified()
    e.Update()
    e.SaveAs(home + "/AsymmPT" + polarity + ".png");
    
    #values obtained for every PHI and ETA Bin
    for i in xrange(sliceNumber+1):
        #variables used to integrate over other variables
        JPsiKinPlusTotETA = 0;
        JPsiKinPlusAssocTotETA = 0;
        JPsiKinMinusTotETA = 0;
        JPsiKinMinusAssocTotETA = 0;    
        
        JPsiKinPlusTotPHI = 0;
        JPsiKinPlusAssocTotPHI = 0;
        JPsiKinMinusTotPHI = 0;
        JPsiKinMinusAssocTotPHI = 0;   
        #loop to integrate over PT slices
        for j in xrange(sliceNumber):    
            for k in xrange(sliceNumber):
                ##open files with 
                JPsiKinMinus = TH2F();
                file1.GetObject("JPsiKin"+ polarity + "Plus"+ str(PTSlicesD["PTlow"+str(k)]),JPsiKinMinus);
               
                JPsiKinPlus = TH2F();
                file2.GetObject("JPsiKin"+ polarity + "Minus" + str(PTSlicesD["PTlow"+str(k)]), JPsiKinPlus);
               
                JPsiKinMinusAssoc = TH2F();
                file1.GetObject("JPsiKinAssoc"+ polarity + "Plus"+ str(PTSlicesD["PTlow"+str(k)]),JPsiKinMinusAssoc);
               
                JPsiKinPlusAssoc = TH2F();
                file2.GetObject("JPsiKinAssoc"+ polarity + "Minus" + str(PTSlicesD["PTlow"+str(k)]),JPsiKinPlusAssoc);
                
                #integrate over every other variable
                JPsiKinPlusTotPHI = JPsiKinPlusTotPHI + JPsiKinPlus.GetBinContent(i,j);
                JPsiKinMinusTotPHI = JPsiKinMinusTotPHI + JPsiKinMinus.GetBinContent(i,j);
                JPsiKinPlusAssocTotPHI = JPsiKinPlusAssocTotPHI + JPsiKinPlusAssoc.GetBinContent(i,j);
                JPsiKinMinusAssocTotPHI = JPsiKinMinusAssocTotPHI + JPsiKinMinusAssoc.GetBinContent(i,j);
        
                JPsiKinPlusTotETA = JPsiKinPlusTotETA + JPsiKinPlus.GetBinContent(j,i);
                JPsiKinMinusTotETA = JPsiKinMinusTotETA + JPsiKinMinus.GetBinContent(j,i);
                JPsiKinPlusAssocTotETA = JPsiKinPlusAssocTotETA + JPsiKinPlusAssoc.GetBinContent(j,i);
                JPsiKinMinusAssocTotETA = JPsiKinMinusAssocTotETA + JPsiKinMinusAssoc.GetBinContent(j,i);
        #if no entries are present in the integrated PHI bin the asymm and error is set to zero
        if ((JPsiKinPlusTotPHI) == 0):
            AsymmJPHI = 0;
            errorAsymmPHI = 0;
        else:
            #if entries are present asymm and error calculated in each PHI slice
            efficiencyPlusPHI = JPsiKinPlusAssocTotPHI/JPsiKinPlusTotPHI
            efficiencyMinusPHI = JPsiKinMinusAssocTotPHI/JPsiKinMinusTotPHI
            
            AsymmJPHI = (efficiencyPlusPHI-efficiencyMinusPHI)/(efficiencyPlusPHI+efficiencyMinusPHI);
            print(AsymmJPHI);
            errorEPlusSQUAREPHI = efficiencyPlusPHI*(1-efficiencyPlusPHI)/JPsiKinPlusTotPHI;
            errorEMinusSQUAREPHI = efficiencyMinusPHI*(1-efficiencyMinusPHI)/JPsiKinMinusTotPHI;
    
            errorNomPHI = 4*(efficiencyPlusPHI*efficiencyPlusPHI*errorEMinusSQUAREPHI + efficiencyMinusPHI*efficiencyMinusPHI*errorEPlusSQUAREPHI);
            BetaPHI = efficiencyPlusPHI + efficiencyMinusPHI;
            errorDenPHI = BetaPHI * BetaPHI * BetaPHI * BetaPHI;
            errorAsymmPHI = errorNomPHI/errorDenPHI;
        #if no entries are present in the integrated ETA bin the asymm and error is set to zero
        if ((JPsiKinPlusTotETA) == 0):
            AsymmJETA = 0;
            errorAsymmETA = 0;
        else:
            #if entries are present asymm and error calculated in each ETA slice
            efficiencyPlusETA = JPsiKinPlusAssocTotETA/JPsiKinPlusTotETA
            efficiencyMinusETA = JPsiKinMinusAssocTotETA/JPsiKinMinusTotETA
    
            AsymmJETA = (efficiencyPlusETA-efficiencyMinusETA)/(efficiencyPlusETA+efficiencyMinusETA);
            print(AsymmJETA);
            errorEPlusSQUAREETA = efficiencyPlusETA*(1-efficiencyPlusETA)/JPsiKinPlusTotETA;
            errorEMinusSQUAREETA = efficiencyMinusETA*(1-efficiencyMinusETA)/JPsiKinMinusTotETA;
    
            errorNomETA = 4*(efficiencyPlusETA*efficiencyPlusETA*errorEMinusSQUAREETA + efficiencyMinusETA*efficiencyMinusETA*errorEPlusSQUAREETA);
            BetaETA = efficiencyPlusETA + efficiencyMinusETA;
            errorDenETA = BetaETA * BetaETA * BetaETA * BetaETA;
            errorAsymmETA = errorNomETA/errorDenETA
        #histograms filled with one dimensional asymmetries for eta and phi created
        AsymmPHI.SetBinContent(i,AsymmJPHI*100);
        AsymmPHI.SetBinError(i, np.sqrt(errorAsymmPHI)*100);
                       
        AsymmETA.SetBinContent(i,AsymmJETA*100);
        AsymmETA.SetBinError(i, np.sqrt(errorAsymmETA)*100);
    #histogram filled with one dimensional asymmetries for eta plotted
    AsymmETA.SetTitle("");
    AsymmETA.SetLineWidth(2);
    #AsymmETA.SetAxisRange(etalow, etahigh, "X");
    (AsymmETA.GetXaxis()).SetTitle("\eta")
    (AsymmETA.GetYaxis()).SetTitle("Asymm[%]")
    (AsymmETA.GetXaxis()).SetTitleSize(0.058)
    (AsymmETA.GetYaxis()).SetTitleSize(0.058)
    (AsymmETA.GetXaxis()).SetLabelSize(0.058)
    (AsymmETA.GetYaxis()).SetLabelSize(0.058)
    AsymmETA.Write()
    f  = TCanvas("AsymmETA", "AsymmETA", 0, 650, 750, 500);
    pad1 = TPad("pad", "pad", 0.03, 0.06, 0.97, 1.)
    pad1.Draw()
    pad1.cd()
    AsymmETA.Draw();
    f.Modified()
    f.Update()
    f.SaveAs(home + "/AsymmETA" + polarity + ".png");
    
    #histogram filled with one dimensional asymmetries for phi plotted
    AsymmPHI.SetTitle("");
    AsymmPHI.SetLineWidth(2);
    #AsymmPHI.SetAxisRange(philow, phihigh, "X");
    (AsymmPHI.GetXaxis()).SetTitle("\phi")
    (AsymmPHI.GetYaxis()).SetTitle("Asymm[%]")
    (AsymmPHI.GetXaxis()).SetTitleSize(0.058)
    (AsymmPHI.GetYaxis()).SetTitleSize(0.058)
    (AsymmPHI.GetXaxis()).SetLabelSize(0.058)
    (AsymmPHI.GetYaxis()).SetLabelSize(0.058)
    AsymmPHI.Write()
    g = TCanvas("AsymmPHI", "AsymmPHI", 0, 650, 750, 500);
    pad2 = TPad("pad", "pad", 0.03, 0.06, 0.97, 1.)
    pad2.Draw()
    pad2.cd()
    AsymmPHI.Draw();
    g.Modified()
    g.Update()
    g.SaveAs(home + "/AsymmPHI" + polarity + ".png");